.. _changelog:

################################################################################
Changelog
################################################################################

v2.0.1
------------------------------------------------------------

:Date: 2025.02.26

Docker file has been updated with python base image 3.13-slim-bullseye and gcc installation without
the recommended packages to reduce the total image size.

v2.0.0
------------------------------------------------------------

:Date: 2024.12.19

The project has been totally rewritten in a more pythonic way.

v1.0.0
------------------------------------------------------------

:Date: 2019.10.19

First historical release.
