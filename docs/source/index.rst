.. STM32 Project Generator documentation master file, created by
   sphinx-quickstart on Fri Dec 13 10:17:22 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

STM32 Project Generator documentation
=====================================

:Release: |version|
:Date: |today|

.. image:: https://gitlab.com/kadriane/stm32-project-generator/badges/master/pipeline.svg

.. image:: https://gitlab.com/kadriane/stm32-project-generator/-/badges/release.svg

.. image:: https://gitlab.com/kadriane/stm32-project-generator/badges/master/coverage.svg


This reference manual describes STM32 Template Project generator API. For learning how to use,
look at :ref:`quickstart <quickstart>`.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   api
   changelog


Indices & Tables
==================

* :ref:`genindex`
* :ref:`modindex`
