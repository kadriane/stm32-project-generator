.. _quickstart:

################################################################################
 Quickstart
################################################################################

Minimum Requirements
======================================================================

- **Linux OS** - currently, generated makefiles are not compatible with Windows OS.
- **Python 3.10** - required minimum version.
- OpenOCD 0.10.1
- GDB-Multiarch 8.1
- ARM-NONE-EABI-GCC 6.3.1
- **Downloaded and extracted STM32 HAL packages with LL HAL files for Fx series**

Installation
======================================================================

To install STM32 Project Generator, run the following script in the shell:

.. code::

    python3 -m pip install git+https://gitlab.com/kadriane

or checkout repository and install from a local folder:

.. code::

    git clone --branch <VERSION> https://gitlab.com/kadriane
    python3 -m pip install stm32-project-generator

The package is installed and available on the $PATH as `stm32projgen`, to see all command, run `--help`` message:

.. code::

    usage: stm32projgen [-h] [-n PROJECT_NAME] [-d] [-f] [-p PROJECT_PATH] [-ide {vscode,visual}] [--test-compilation] mcu

    STM32 Project Generator v2.0.1
    Template project generator for STM32 and other micro-controllers

    Example: stm32projgen stm32f103c8t6 -n my_project -p /tmp/

    positional arguments:
      mcu                   full MCU name, e.g "stm32f103c8t6".

    options:
      -h, --help            show this help message and exit
      -n PROJECT_NAME, --project-name PROJECT_NAME
                            Full project name.
      -d, --debug           Enable printing debug message on the STDOUT stream.
      -f, --force           force files overwriting during generation if the project exists. Moreover, if enabled, skip checking the required project dependiencies.
      -p PROJECT_PATH, --project-path PROJECT_PATH
                            Project path (POSIX) where project will be created (default: ./out).
      -ide {vscode,visual}, --ide {vscode,visual}
                            select IDE (currently only configuration for VSC is available).
      --test-compilation    Enable test compilation for generated project.

    Karol Adrianek (gitlab: kadriane), MIT license, 2019-2025


How to use
======================================================================

Based on the help message, to generate a new project:

.. code::

    stm32projgen stm32f103c8t6 -n my_project -p /tmp/


Expected output:

.. code::

    #========================= STM32 Project Generator ===========================#
    # @project_name:      my_project
    # @STM32:             STM32F103C8T6
    # @timestamp:         11:14:37 13/12/2024
    # @author:            kadriane
    #=============================================================================#
    Generating project for MCU "stm32f103c8t6" started.
    Creating project folders in progress...
    # Creating project folders -> done.
    # Generating source files for STM32F103C8T6 done.
    # Project "my_project" created in location: "/tmp/my_project".
    # Test compilation of created project in progress...
    # Test compilation finished successfully.
    # End of program...


Expected output folder:

.. code::

    tree -L 1  /tmp/my_project/
    /tmp/my_project/
    |-- Makefile
    |-- STM32F103C8T6_LINKER.ld
    |-- build
    |-- docs
    |-- drivers
    |-- main.c
    |-- main.h
    |-- st
    `-- startup_stm32f103xb.s


Building, Flashing, and Debugging project
======================================================================

Makefile
------------------------------------------------------------

Go to the generated project folder and type in your console:

- **Building**: `make all`,
- **flashing**: `make flash`,
- **debugging**: `make debug`.

**Note:**
Debugging is based on the `GDB-Multiarch` and `OpenOCD` toolchains.

Visual Studio Code
------------------------------------------------------------

Press `ctrl+shift+b` and select one of the tasks:

.. image:: ../img/vsc_taskslist.png

In order to launch debugger mode, press `F5`:

.. image:: ../img/vsc_debugger.png


Compiling project:

.. code::

    $ make all
    [...]
    arm-none-eabi-size build/stm32f103c8t6.elf
       text	   data	    bss	    dec	    hex	filename
        728	      8	   1568	   2304	    900	build/stm32f103c8t6.elf
    arm-none-eabi-objcopy -O ihex build/stm32f103c8t6.elf build/stm32f103c8t6.hex
    arm-none-eabi-objcopy -O binary -S build/stm32f103c8t6.elf build/stm32f103c8t6.bin


Compiling project and flashing MCU:

.. code::

    $ make flash
    [...]
    ** Programming Started **
    auto erase enabled
    target halted due to breakpoint, current mode: Thread
    xPSR: 0x61000000 pc: 0x2000003a msp: 0x200027fc
    wrote 1024 bytes from file build/stm32f103c8t6.elf in 0.103567s (9.656 KiB/s)
    ** Programming Finished **
    ** Verify Started **
    target halted due to breakpoint, current mode: Thread
    xPSR: 0x61000000 pc: 0x2000002e msp: 0x200027fc
    verified 736 bytes in 0.036955s (19.449 KiB/s)
    ** Verified OK **
    ** Resetting Target **
