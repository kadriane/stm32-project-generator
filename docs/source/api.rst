.. _api:

################################################################################
STM32 Project Generator API
################################################################################

Main Packages, Modules, and Classes
======================================================================

.. rubric:: Packages

.. autosummary::

    stm32projgenerator.generators
    stm32projgenerator.utils

.. rubric:: Main Modules

.. autosummary::

    stm32projgenerator.stm32projgenerator
    stm32projgenerator.generators.defs
    stm32projgenerator.generators.base
    stm32projgenerator.generators.stm32generator

.. rubric:: Main Classes

.. autosummary::

    stm32projgenerator.generators.base.BaseGenerator
    stm32projgenerator.generators.stm32generator.STM32Generator



STM32 Project Generator main module
======================================================================

.. automodule:: stm32projgenerator.stm32projgenerator
    :members:
    :undoc-members:
    :show-inheritance:


Package "generators"
======================================================================

.. automodule:: stm32projgenerator.generators


Module "defs"
------------------------------------------------------------

.. automodule:: stm32projgenerator.generators.defs
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: PKG_PATH,TEMPLATE_PATHS

    .. autodata:: stm32projgenerator.generators.defs.PKG_PATH
        :no-value:

    .. autodata:: TEMPLATE_PATHS
        :no-value:
    

Module "base"
------------------------------------------------------------

.. automodule:: stm32projgenerator.generators.base
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Classes

    .. autosummary::

        BaseGenerator


Module "stm32generator"
------------------------------------------------------------

.. automodule:: stm32projgenerator.generators.stm32generator
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Classes

    .. autosummary::

        STM32Generator


Package "utils"
======================================================================

.. automodule:: stm32projgenerator.utils


Module "fileutils"
------------------------------------------------------------

.. automodule:: stm32projgenerator.utils.fileutils
    :members:
    :undoc-members:
    :show-inheritance:

Module "gccutils"
------------------------------------------------------------

.. automodule:: stm32projgenerator.utils.gccutils
    :members:
    :undoc-members:
    :show-inheritance:

Module "logger"
------------------------------------------------------------

.. automodule:: stm32projgenerator.utils.logger
    :members:
    :undoc-members:
    :show-inheritance:

Module "utils"
------------------------------------------------------------

.. automodule:: stm32projgenerator.utils.utils
    :members:
    :undoc-members:
    :show-inheritance: