'''
Base module for MCU generators.
'''
import logging

from pathlib import Path

class BaseGenerator:
    '''
    Base class generator for MCU generators.
    '''
    def __init__(self, mcu_name: str, **kwargs):
        '''
        Initializes the BaseGenerator.

        :param mcu_name: The name of the MCU.
        :type mcu_name: str
        :param kwargs: Additional keyword arguments.
        :keyword project_path: The path where the project will be generated.
        :type project_path: str
        '''
        self.mcu_name = mcu_name.lower()
        self.logger = logging.getLogger(BaseGenerator.__name__)

        self.project_path = Path(kwargs.get('project_path', f'out/{mcu_name}'))
        self.project_name = self.project_path.name

        if not kwargs.get('project_path'):
            self.logger.warning(f'BaseGenerator: project output path not provided, '
                                f'default: "{self.project_path}"')

    def __repr__(self):
        return f'{self.__class__.__name__}(mcu_name={self.mcu_name})'

    def execute(self, **kwargs):
        '''
        Execute generator to generate a project template.

        :param kwargs: Additional keyword arguments.
        :return: None
        '''
        self.logger.debug(f'BaseGenerator: input kwargs: {kwargs}')
        self.logger.info(f'Generating project for MCU "{self.mcu_name}" started.')
        self.create_project_folder(self.project_path)

    def create_project_folder(self, project_path: Path):
        '''
        Method to create a folder for the generated project.

        :param project_path: The path where the project folder will be created.
        :type project_path: Path
        :return: None
        '''
        self.logger.debug(f'BaseGenerator: create project folder "{project_path}" in progress...')

        if project_path.exists():
            self.logger.warning('BaseGenerator: project folder already exists - skip, continue...')
            return

        project_path.mkdir(parents=True, exist_ok=True)
        self.logger.debug(f'BaseGenerator: creating project folder "{project_path}" -> done.')
