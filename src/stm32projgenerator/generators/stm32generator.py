'''
STM32 Project Generator class for STM32 MCUs.
'''
import logging

from pathlib import Path

from stm32projgenerator.generators.defs import MCUConfig
from stm32projgenerator.generators.defs import FilesConfig
from stm32projgenerator.generators.defs import TEMPLATE_PATHS

from stm32projgenerator.generators.base import BaseGenerator

from stm32projgenerator.utils import utils
from stm32projgenerator.utils import fileutils


# RE Patterns for the model, line and series retrieved from provided MCU name
STM32_MCU_CFG_REGEX = {
    'model': [
        r'\w+c(\d)t\d',
        r'\w+v([a-z])t\d'
    ],
    'line': [
        r'\w+(\d\d)c',
        r'\w+(\d\d)v[a-z]'
    ],
    'series': [
        r'32([fFgGhHlL]\d)\d\d',
        r'32(m\d)\w+',
        r'32([lL]\d)\w+',
        r'32([wW][bB]0)\w+',
        r'32([wW][bB])\w+'
    ]
}

STM32_LINUX_REQUIRED_EXECS = [
    'arm-none-eabi-gcc',
    'openocd',
    'gdb-multiarch'
]

STM32_PROJ_FOLDERS = {
    'project': '',
    'visual': '.vscode',
    'doc': 'docs',

    'drivers': 'drivers',
    'drivers_inc': 'drivers/inc',
    'drivers_src': 'drivers/src',

    'st': 'st',
    'st_inc': 'st/inc',
    'st_legacy_inc': 'st/inc/Legacy',
    'st_src': 'st/src',
    'st_core': 'st/core'
}


class STM32Generator(BaseGenerator):
    '''
    STM32 MCU Project Generator.

    This class extends the :class:`BaseGenerator` to provide specific functionality for generating
    STM32 MCU projects.
    '''
    def __init__(self, mcu_name: str, **kwargs):
        """
        Initializes the STM32Generator.

        :param mcu_name: The name of the MCU.
        :type mcu_name: str
        :param kwargs: Additional keyword arguments.
        :return: None
        """
        super().__init__(mcu_name, **kwargs)
        self.logger = logging.getLogger(STM32Generator.__name__)

        make_cfg = utils.load_yaml_file(TEMPLATE_PATHS['stm32']['gccmake'])
        valid_mcus = make_cfg.keys()
        if not self.mcu_name in valid_mcus:
            raise KeyError(f'STM32Generator: provided MCU name "{self.mcu_name}" not supported'
                           f'(available="{valid_mcus}").')

        self.build_cfg = make_cfg[self.mcu_name]

    @staticmethod
    def parse_mcu_name(mcu_name: str) -> MCUConfig:
        '''
        Parse MCU name into MCUConfig namedtuple with MCU parameters.

        :param mcu_name: The name of the MCU.
        :type mcu_name: str
        :return: MCUConfig namedtuple containing the parsed MCU parameters.
        :rtype: MCUConfig
        '''
        mcu_name = mcu_name.lower()

        data = {'family_name': 'stm32'}
        data['fullname'] = mcu_name
        data['model'] = utils.find_substring(mcu_name, STM32_MCU_CFG_REGEX['model'])
        data['line'] = utils.find_substring(mcu_name, STM32_MCU_CFG_REGEX['line'])
        data['series'] = utils.find_substring(mcu_name, STM32_MCU_CFG_REGEX['series'])

        return MCUConfig(**data)

    def _generate_ide_files(self):
        self.logger.debug('# Generating IDE (VSCode) config files in progress...')
        files_config = {
            'json_task': FilesConfig(srcpath=TEMPLATE_PATHS['visual'],
                                    destpath=STM32_PROJ_FOLDERS['visual'],
                                    filter_pattern='stm32TaskJson.tmpl',
                                    templ_cfg=None,
                                    filename='tasks.json'),

            'json_launch': FilesConfig(srcpath=TEMPLATE_PATHS['visual'],
                                      destpath=STM32_PROJ_FOLDERS['visual'],
                                      filter_pattern='stm32LaunchJson.tmpl',
                                      templ_cfg=self.build_cfg['makefile'],
                                      filename='launch.json'),

            'json_settings': FilesConfig(srcpath=TEMPLATE_PATHS['visual'],
                                        destpath=STM32_PROJ_FOLDERS['visual'],
                                        filter_pattern='stm32SettingsJson.tmpl',
                                        templ_cfg=None,
                                        filename='settings.json'),

            'gitignore': FilesConfig(srcpath=TEMPLATE_PATHS['git'],
                                     destpath=STM32_PROJ_FOLDERS['project'],
                                     filter_pattern='*gitignore*',
                                     templ_cfg=None,
                                     filename='.gitignore'),
        } # files_config

        for _, params in files_config.items():
            fileutils.generate_files(params, self.project_path)

        self.logger.debug('# The IDE configuration files for VSCode customized.')

    def _generate_vendor_files(self, mcu_cfg: MCUConfig):
        self.logger.debug(f'# Getting vendor libs files configuration for MCU "{self.mcu_name}"'
                          ' in progress...')
        stm32line = mcu_cfg.series
        pkg_path = TEMPLATE_PATHS['stm32']['libs']
        stm32line_pkg_path = f'{pkg_path}/stm32{stm32line.lower()}xx_hal_driver'
        cmsis_path = f'{pkg_path}/cmsis_device_{stm32line.lower()}'

        mcu_model_name = self.build_cfg["makefile"]["MCU_MODEL_DEFINE"]
        mcu_model_name = mcu_model_name.lower()

        if not Path(pkg_path).exists():
            errmsg = f'# Incorrect vendor package path "{pkg_path}" - not exist.'
            self.logger.error(errmsg)
            raise RuntimeError(errmsg)

        # Files paths based on the ST HAL packages
        files_config = {
            'hal_inc': FilesConfig(srcpath=f'{stm32line_pkg_path}/Inc/',
                                  destpath=STM32_PROJ_FOLDERS['st_inc'],
                                  filter_pattern='*.h'),

            'hal_legacy_inc': FilesConfig(srcpath=f'{stm32line_pkg_path}/Inc/Legacy/',
                                        destpath=STM32_PROJ_FOLDERS['st_legacy_inc'],
                                        filter_pattern='*.h'),

            'hal_inc_conf': FilesConfig(srcpath=f'{stm32line_pkg_path}/Inc/',
                                      destpath=STM32_PROJ_FOLDERS["st_inc"],
                                      filter_pattern='*conf_template*.h',
                                      filename=f'stm32{stm32line.lower()}xx_hal_conf.h'),

            'hal_src': FilesConfig(srcpath=f'{stm32line_pkg_path}/Src/',
                                  destpath=STM32_PROJ_FOLDERS['st_src'],
                                  filter_pattern='*_ll_*.c'),

            'cmsis_inc': FilesConfig(srcpath=f'{pkg_path}/cmsis_core/Include/',
                                    destpath=STM32_PROJ_FOLDERS['st_core'],
                                    filter_pattern='*.h'),

            'system_src': FilesConfig(srcpath=f'{cmsis_path}/Source/Templates/',
                                     destpath=STM32_PROJ_FOLDERS['st'],
                                     filter_pattern=f'system_stm32{stm32line.lower()}xx.c'),

            'system_inc': FilesConfig(srcpath=f'{cmsis_path}/Include/',
                                     destpath=STM32_PROJ_FOLDERS['st'],
                                     filter_pattern=f'system_stm32{stm32line.lower()}xx.h'),

            'line_header': FilesConfig(srcpath=f'{cmsis_path}/Include/',
                                      destpath=STM32_PROJ_FOLDERS['st'],
                                      filter_pattern=f'stm32{stm32line.lower()}xx.h'),

            'stm32fxx_inc': FilesConfig(srcpath=f'{cmsis_path}/Include/',
                                       destpath=STM32_PROJ_FOLDERS['st'],
                                       filter_pattern=f'*{mcu_model_name}*.h'),

            'startup': FilesConfig(srcpath=f'{cmsis_path}/Source/Templates/gcc/',
                                   destpath=STM32_PROJ_FOLDERS['project'],
                                   filter_pattern=f'*{mcu_model_name}*.s')
        } # files_config

        for _, params in files_config.items():
            fileutils.generate_files(params, self.project_path)

    def _generate_template_files(self, mcu_cfg: MCUConfig, force: bool = False):
        rootpath = TEMPLATE_PATHS[mcu_cfg.family_name]['src']

        # Files paths based on the ST HAL packages
        files_config = {
            'linker': FilesConfig(srcpath=rootpath,
                                  destpath=STM32_PROJ_FOLDERS['project'],
                                  filter_pattern='STM32XXXX_LINKER.ld',
                                  templ_cfg=self.build_cfg['linker'],
                                  filename=self.build_cfg['makefile']['LD_SCRIPT_NAME'],
                                  precompile_file=True),

            'templ_main': FilesConfig(srcpath=rootpath,
                                     destpath=STM32_PROJ_FOLDERS['project'],
                                     filter_pattern='*main*'),

            'templ_hw': FilesConfig(srcpath=rootpath,
                                    destpath=STM32_PROJ_FOLDERS['drivers'],
                                    filter_pattern='*hw_*'),
        } # files_config

        for _, params in files_config.items():
            fileutils.generate_files(params, self.project_path, force)

    def _generate_makefile(self, mcu_cfg: MCUConfig, force: bool = False):
        templpath = TEMPLATE_PATHS[mcu_cfg.family_name]['src']

        c_sources_str = ''
        includes_str = ''
        for folder, path in STM32_PROJ_FOLDERS.items():
            if folder in ('project', 'doc', 'visual'):
                continue
            c_sources_str += f'C_SOURCES += $(shell find {path} -name "*.c")\n'
            includes_str += f'C_INCLUDES += -I{path}/\n'

        cfg = self.build_cfg['makefile']
        cfg['C_SOURCES'] = c_sources_str
        cfg['C_INCLUDES'] = includes_str
        makefile = FilesConfig(srcpath=templpath,
                               destpath=STM32_PROJ_FOLDERS['project'],
                               filter_pattern='Makefile',
                               templ_cfg=cfg)

        fileutils.generate_files(makefile, self.project_path, force)

    def execute(self, **kwargs):
        """
        Execute the STM32Generator to generate a project template.

        This method extends the :meth:`BaseGenerator.execute` method by adding specific
        functionality for STM32 MCU projects.

        :param kwargs: Additional keyword arguments.
        :keyword force: Force execution even if required executables are missing.
        :type force: bool
        :return: None
        """
        super().execute(**kwargs)
        self.logger.debug(f'STM32Gen {self.mcu_name}, {kwargs}')

        force = kwargs.get('force', False)
        execs_ok = utils.check_executables(STM32_LINUX_REQUIRED_EXECS)

        if not execs_ok and not force:
            raise RuntimeError('STM32Generator: missing required executables - abort.')

        if not execs_ok and force:
            self.logger.warning('STM32Generator: missing required executables: ignored for '
                                '"--force" flag - continue...')

        mcucfg = self.parse_mcu_name(self.mcu_name)
        self.logger.debug(f'STM32Generator: MCU Config = "{mcucfg}"')

        fileutils.generate_folders(STM32_PROJ_FOLDERS, self.project_path)

        self._generate_ide_files()
        self._generate_vendor_files(mcucfg)
        self._generate_template_files(mcucfg, force=force)
        self._generate_makefile(mcucfg, force=force)
