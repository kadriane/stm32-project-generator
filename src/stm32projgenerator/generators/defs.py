'''
Definitions and constants for MCU generators.
'''
import importlib.resources

from collections import namedtuple

import stm32projgenerator

#: The path to the package resources.
PKG_PATH = importlib.resources.files(stm32projgenerator)

#: Dictionary of template paths used in the project.
#:
#: - **git**: Path to Git templates.
#: - **visual**: Path to Visual Studio templates.
#: - **stm32**: Dictionary of paths for STM32 templates.
#:     - **src**: Path to STM32 source templates.
#:     - **libs**: Path to STM32 libraries.
#:     - **gccmake**: Path to STM32 GCC build configuration.
TEMPLATE_PATHS = {
    'git': f'{PKG_PATH}/templates/git/',
    'visual': f'{PKG_PATH}/templates/visual/',
    'stm32': {
        'src': f'{PKG_PATH}/templates/stm32/',
        'libs': f'{PKG_PATH}/thirdparty/stm32/',
        'gccmake': f'{PKG_PATH}/config/stm32buildcfg.yaml'
    }
}

#: List of supported Integrated Development Environments (IDEs).
IDEs = ['vscode', 'visual']

mcu_config_fields = (
    'family_name',
    'fullname',
    'series',
    'line',
    'model'
)

#: Named tuple representing MCU configuration.
#:
#: - **family_name**: The family name of the MCU.
#: - **fullname**: The full name of the MCU.
#: - **series**: The series of the MCU.
#: - **line**: The line of the MCU.
#: - **model**: The model of the MCU.
MCUConfig = namedtuple('MCUConfig',
                       mcu_config_fields,
                       defaults = (None,)*len(mcu_config_fields))

files_config_fields = (
    'srcpath',
    'destpath',
    'filter_pattern',
    'templ_cfg',
    'filename',
    'precompile_file'
)

#: Named tuple representing file configuration parameters.
#:
#: - **srcpath**: The source path of the file.
#: - **destpath**: The destination path of the file.
#: - **filter_pattern**: The filter pattern for the file.
#: - **templ_cfg**: The template configuration for the file.
#: - **filename**: The filename.
#: - **precompile_file**: Whether the file should be precompiled.
FilesConfig = namedtuple('FilesParams',
                         files_config_fields,
                         defaults=(None,)*len(files_config_fields))
