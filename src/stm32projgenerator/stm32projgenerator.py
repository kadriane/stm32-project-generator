'''
STM32 Project Generator
'''
import sys
import argparse
import getpass
import logging
import importlib
import importlib.metadata

from datetime import datetime
from pathlib import Path

from stm32projgenerator.utils.gccutils import test_compilation

from stm32projgenerator.utils.logger import logger_setup

from stm32projgenerator.generators.defs import IDEs
from stm32projgenerator.generators.base import BaseGenerator
from stm32projgenerator.generators.stm32generator import STM32Generator


LOGGER_FILE_NAME = f"{__package__}.log"
LOGGER_PARSER_NAME = '__parser__'
LOGGER_MAIN_NAME = '__main__'

_GENERATORS = {
    'stm32': STM32Generator,
}


def generator_instance(mcu_name: str, **kwargs) -> BaseGenerator:
    '''
    Return Generator instance for provided MCU name, or raises an ValueError.

    :param mcu_name: The name of the MCU for which to create a generator instance.
    :type mcu_name: str
    :param kwargs: Additional keyword arguments to be passed to the generator's constructor.

    :return: An instance of the appropriate generator class for the specified MCU.
    :rtype: BaseGenerator

    :raises ValueError: If the specified MCU name does not match any supported MCU families.
    '''
    for key, gen in _GENERATORS.items():
        if mcu_name.startswith(key):
            return gen(mcu_name, **kwargs)
    raise ValueError(f'MCU family "{mcu_name}" not supported')


def parse_args():
    '''
    STM32 Project Generator CLI parser.

    :return: A namespace object containing the parsed command-line arguments.
    :rtype: argparse.Namespace

    :raises SystemExit: If the command-line arguments are invalid or if help is requested.
    '''
    pkginfo = importlib.metadata.metadata(__package__)

    desc = f'STM32 Project Generator v{pkginfo['version']}\n'
    desc += pkginfo['summary']
    desc += "\n\nExample: stm32projgen stm32f103c8t6 -n my_project -p /tmp/"
    epilog = f'{pkginfo["author"]} (gitlab: kadriane), MIT license, 2019-2025.'

    parser = argparse.ArgumentParser(description=desc,
                                    epilog=epilog,
                                    formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('mcu', type=str, help='full MCU name, e.g "stm32f103c8t6".')

    # MCU name is a mandatory part, and the len check is only required to not crash the program
    # When no arg is provided.
    parser.add_argument('-n', '--project-name',
                        type=str,
                        default=str(len(sys.argv) >= 2 and sys.argv[1]),
                        help='Full project name.')

    parser.add_argument('-d', '--debug',
                        action='store_true',
                        help='Enable printing debug message on the STDOUT stream.')

    parser.add_argument('-f', '--force',
                        action='store_true',
                        help='force files overwriting during generation if the project exists. '
                             'Moreover, if enabled, skip checking the required project '
                             'dependiencies.')

    parser.add_argument('-p', '--project-path',
                        type=Path,
                        default=Path('out/'),
                        help='Project path (POSIX) where project will be created (default: ./out).')

    parser.add_argument('-ide', '--ide',
                        type=str,
                        choices=IDEs,
                        dest='ide',
                        default=None,
                        help='select IDE (currently only configuration for VSC is available).')

    parser.add_argument('--test-compilation',
                        action='store_true',
                        help='Enable test compilation for generated project.')

    return parser.parse_args()



def project_info(mcu_name: str, project_name: str):
    '''
    Print project meta info to stdout.

    :param mcu_name: The name of the STM32 microcontroller unit (MCU) used in the project.
    :type mcu_name: str
    :param project_name: The name of the project being generated.
    :type project_name: str
    '''
    logger = logging.getLogger(LOGGER_MAIN_NAME)
    ts = datetime.now().timetuple()

    logger.info('#========================= STM32 Project Generator ===========================#')
    logger.info(f'# @project_name:      {project_name}')
    logger.info(f'# @STM32:             {mcu_name.upper()}')
    logger.info(f'# @timestamp:         {ts.tm_hour}:{ts.tm_min}:{ts.tm_sec} '
                f'{ts.tm_mday}/{ts.tm_mon}/{ts.tm_year}')
    logger.info(f'# @author:          {getpass.getuser()}')
    logger.info('#=============================================================================#')


def main():
    '''
    Main function of STM32 Project Generator.

    This function serves as the entry point for the STM32 Project Generator CLI application.
    It sets up logging, parses command-line arguments, and orchestrates the project generation 
    process. The function handles project configuration based on user input, including 
    whether to force overwrite existing files and whether to skip test compilation.

    The main steps performed by this function include:
    1. Setting up the logger with specified configurations.
    2. Parsing command-line arguments to retrieve user inputs.
    3. Logging project information based on the provided MCU name and project name.
    4. Creating an instance of the appropriate generator for the specified MCU.
    5. Executing the generator to create project files.
    6. Optionally running a test compilation if specified by the user.

    :raises SystemExit: If there are issues with command-line arguments or during execution 
                        that require terminating the program.
    '''
    logger_setup(filename=LOGGER_FILE_NAME)
    logger = logging.getLogger(LOGGER_MAIN_NAME)

    args = parse_args()

    if args.debug:
        logger_setup(filename=LOGGER_FILE_NAME, debug=args.debug)

    project_info(args.mcu, args.project_name)

    if args.force:
        logger.warning('Flag "--force" used - overwrite all files and ignore warnings, continue...')

    project_path = Path(f'{args.project_path}/{args.project_name}/')
    gen = generator_instance(args.mcu, project_path=project_path)

    logger.debug(f'Selected generator: "{gen}", generating in progress...')
    gen.execute(force=args.force)

    logger.info(f'# Generating source files for {args.mcu.upper()} done.')
    logger.info(f'# Project "{args.project_name}" created in location: "{project_path}".')

    if args.test_compilation:
        test_compilation(project_path)

    logger.info('# End of program...')


if __name__ == '__main__':
    main()
