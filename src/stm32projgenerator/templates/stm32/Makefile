##########################################################################################################################

# ------------------------------------------------
# Generic Makefile (based on gcc)
# ------------------------------------------------

######################################
# target
######################################
TARGET = $TARGET

GDB = gdb-multiarch


######################################
# building variables
######################################
# debug build?
DEBUG = 1
# optimization
OPT = -Og


#######################################
# paths
#######################################
# Build path
BUILD_DIR = build

######################################
# source
######################################

# C sources
C_SOURCES = $$(shell find . -name "*.c")
$C_SOURCES

# ASM sources
ASM_SOURCES = $$(shell find . -name "*.s")

#######################################
# binaries
#######################################
PREFIX = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $$(GCC_PATH)/$$(PREFIX)gcc
AS = $$(GCC_PATH)/$$(PREFIX)gcc -x assembler-with-cpp
CP = $$(GCC_PATH)/$$(PREFIX)objcopy
SZ = $$(GCC_PATH)/$$(PREFIX)size
else
CC = $$(PREFIX)gcc
AS = $$(PREFIX)gcc -x assembler-with-cpp
CP = $$(PREFIX)objcopy
SZ = $$(PREFIX)size
endif
HEX = $$(CP) -O ihex
BIN = $$(CP) -O binary -S
 
#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=$MCU_CPU

# fpu
FPU = $MCU_FPU

# float-abi
FLOAT-ABI = $MCU_FLOAT_ABI

# mcu
MCU = $$(CPU) -mthumb $$(FPU) $$(FLOAT-ABI)

# macros for gcc
# AS defines
AS_DEFS = 

# C defines
C_DEFS =  \
-DUSE_FULL_LL_DRIVER \
-D$MCU_MODEL_DEFINE \
-D$MCU_LINE_DEFINE	

# AS includes
AS_INCLUDES = 

# C includes
C_INCLUDES = -I.
$C_INCLUDES


# compile gcc flags
ASFLAGS = $$(MCU) $$(AS_DEFS) $$(AS_INCLUDES) $$(OPT) -Wall -fdata-sections -ffunction-sections

CFLAGS = $$(MCU) $$(C_DEFS) $$(C_INCLUDES) $$(OPT) -Wall -fdata-sections -ffunction-sections

ifeq ($$(DEBUG), 1)
CFLAGS += -g -gdwarf-2
endif


# Generate dependency information
CFLAGS += -MMD -MP -MF"$$(@:%.o=%.d)"


#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = $LD_SCRIPT_NAME

# libraries
LIBS = -lc -lm -lnosys 
LIBDIR = 
LDFLAGS = $$(MCU) -specs=nano.specs -T$$(LDSCRIPT) $$(LIBDIR) $$(LIBS) -Wl,-Map=$$(BUILD_DIR)/$$(TARGET).map,--cref -Wl,--gc-sections

# default action: build all
all: $$(BUILD_DIR)/$$(TARGET).elf $$(BUILD_DIR)/$$(TARGET).hex $$(BUILD_DIR)/$$(TARGET).bin


#######################################
# build the application
#######################################
# list of objects
OBJECTS = $$(addprefix $$(BUILD_DIR)/,$$(notdir $$(C_SOURCES:.c=.o)))
vpath %.c $$(sort $$(dir $$(C_SOURCES)))

# list of ASM program objects
OBJECTS += $$(addprefix $$(BUILD_DIR)/,$$(notdir $$(ASM_SOURCES:.s=.o)))
vpath %.s $$(sort $$(dir $$(ASM_SOURCES)))

$$(BUILD_DIR)/%.o: %.c Makefile | $$(BUILD_DIR) 
	$$(CC) -c $$(CFLAGS) -Wa,-a,-ad,-alms=$$(BUILD_DIR)/$$(notdir $$(<:.c=.lst)) $$< -o $$@

$$(BUILD_DIR)/%.o: %.s Makefile | $$(BUILD_DIR)
	$$(AS) -c $$(CFLAGS) $$< -o $$@

$$(BUILD_DIR)/$$(TARGET).elf: $$(OBJECTS)
	$$(CC) $$^ $$(LDFLAGS) $$(LIBS) -o $$@
	$$(SZ) $$@

$$(BUILD_DIR)/%.hex: $$(BUILD_DIR)/%.elf | $$(BUILD_DIR)
	$$(HEX) $$< $$@
	
$$(BUILD_DIR)/%.bin: $$(BUILD_DIR)/%.elf | $$(BUILD_DIR)
	$$(BIN) $$< $$@	
	
$$(BUILD_DIR):
	mkdir $$@		

#######################################
# clean up
#######################################
clean:
	-rm -fR $$(BUILD_DIR)

#######################################
# FLashing & Debugger
#######################################

flash: $$(BUILD_DIR)/$$(TARGET).elf
	openocd -f interface/stlink-v2.cfg -f target/$OPENOCD_TARGET_CFG \
		-c "init; flash probe 0; program $$^ 0 verify reset; exit"

debug_ocd: $$(BUILD_DIR)/$$(TARGET).elf
	openocd -f interface/stlink-v2.cfg -f target/$OPENOCD_TARGET_CFG -c "init" &

debug_gdb: $$(BUILD_DIR)/$$(TARGET).elf
	$$(GDB) -ex "target remote localhost:3333" -ex "monitor reset halt" $$^
	killall openocd

debug: all flash debug_ocd debug_gdb
  
#######################################
# dependencies
#######################################
-include $$(wildcard $$(BUILD_DIR)/*.d)

# *** EOF ***
