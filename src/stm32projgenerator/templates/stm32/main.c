/**
  ******************************************************************************
  * @file    main.c 
  * @brief   Main program file
  ******************************************************************************
  **/

#include "main.h"

#ifdef STM32F1XX_EXAMPLE
#include <st/stm32f1xx.h>
#include <st/system_stm32f1xx.h>

#include <st/inc/stm32f1xx_ll_bus.h>
#include <st/inc/stm32f1xx_ll_gpio.h>

// Example for STM32F1 Blue Pill
#define LED_GPIO_PORT	GPIOC
#define LED_GPIO_MASK	LL_GPIO_PIN_13

static void system_clk_init(void)
{
	SystemInit();
	SystemCoreClockUpdate();

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
}
static void led_gpio_init(void)
{
	LL_GPIO_SetPinMode(LED_GPIO_PORT, LED_GPIO_MASK, LL_GPIO_MODE_OUTPUT);
	LL_GPIO_SetPinSpeed(LED_GPIO_PORT, LED_GPIO_MASK, LL_GPIO_SPEED_FREQ_HIGH);
	LL_GPIO_SetPinOutputType(LED_GPIO_PORT, LED_GPIO_MASK, LL_GPIO_OUTPUT_PUSHPULL);
}

static void sleep(uint32_t tics)
{
	uint32_t i = tics;
	while (--i)
	{
		__ASM volatile ("NOP");
	};
}

static void led_gpio_togglebit(void)
{
	LL_GPIO_TogglePin(LED_GPIO_PORT, LED_GPIO_MASK);
	sleep(1000000);
}
#endif

int main (void)
{
	hw_system_init();

#ifdef STM32F1XX_EXAMPLE
	system_clk_init();
	led_gpio_init();

    while (1)
	{
		led_gpio_togglebit();
	}
#endif
}

/*******************************END OF FILE************************************/
