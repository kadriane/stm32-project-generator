#ifndef HW_SYSTEMCONFIG_H_
#define HW_SYSTEMCONFIG_H_

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

#define HW_SYSTEM_REG_IMPLELEMTATION							0x00
#define HW_SYSTEM_REG_IMPLELEMTATION_LCD						0x01

#define HW_SYSTICK_FREQ											2000

/*******************************************************************************
  * Tests controls macros
  ******************************************************************************/

#define HW_SYSTEM_TEST											0x01

#define HW_SYSTEM_TEST_TIMEOUT									0xA00

#define HW_SYSTEM_TEST_NOR										0x00
#define HW_SYSTEM_TEST_SDRAM									0x00

#define HW_SYSTEM_TEST_GPIO_SPIs								0x00
#define HW_SYSTEM_TEST_GPIO_LTDC								0x00
#define HW_SYSTEM_TEST_GPIO_NOR_SDRAM							0x00

#define HW_SYSTEM_TEST_SPIs_UNITTESTs							0x00
#define HW_SYSTEM_TEST_SPIs_DMA_UNITTESTs						0x01
#define HW_SYSTEM_TEST_SPIs_ADVANCEDTESTs						0x00

#define HW_SYSTEM_TEST_LCD										0x01


/*******************************************************************************
  * SPIs buses parameters
  ******************************************************************************/
/**
 * HW SPI1 peripherals register and I/O ports assignments
 */
#define HW_SPI1													SPI6
#define HW_SPI1_RCC_CLK											RCC_APB2Periph_SPI6
#define HW_SPI1_RCC_CLK_CMD_INIT								RCC_APB2PeriphClockCmd
#define HW_SPI1_IRQn											SPI6_IRQn
#define HW_SPI1_IRQnHANDLER										SPI6_IRQHandler
#define HW_SPI1_IRQn_RX_FLAG									SPI_I2S_IT_RXNE
#define HW_SPI1_IRQn_TX_FLAG									SPI_I2S_IT_TXE
#define HW_SPI1_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_1
#define HW_SPI1_NVIC_PREEMPTION_PRIORITY						0x1
#define HW_SPI1_NVIC_SUB_PRIORITY								0x1

#define HW_SPI1_GPIO_PORT										GPIOG
#define HW_SPI1_GPIO_AF											GPIO_AF_SPI6
#define HW_SPI1_GPIO_MOSI_PIN									GPIO_Pin_14
#define HW_SPI1_GPIO_MOSI_SOURCE_PIN							GPIO_PinSource14
#define HW_SPI1_GPIO_MISO_PIN									GPIO_Pin_12
#define HW_SPI1_GPIO_MISO_SOURCE_PIN							GPIO_PinSource12
#define HW_SPI1_GPIO_CLK_PIN									GPIO_Pin_13
#define HW_SPI1_GPIO_CLK_SOURCE_PIN								GPIO_PinSource13

#define HW_SPI1_GPIO_PORT_ID_DIR								GPIOG
#define HW_SPI1_GPIO_DIR_PIN									GPIO_Pin_10
#define HW_SPI1_GPIO_ID_PIN										GPIO_Pin_11
#define HW_SPI1_GPIO_ID_PIN_SOURCE								EXTI_PinSource11
#define HW_SPI1_GPIO_ID_EXTI_LINE								EXTI_Line11

#define HW_SPI1_GPIO_ID_IRQn           							EXTI15_10_IRQn
#define HW_SPI1_GPIO_ID_IRQnHANDLER    							EXTI15_10_IRQHandler
#define HW_SPI1_GPIO_ID_EXTI_SOURCE								EXTI_PortSourceGPIOG
#define HW_SPI1_GPIO_ID_EXTI_TRIGGER_TYPE						EXTI_Trigger_Rising
#define HW_SPI1_GPIO_ID_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_SPI1_GPIO_ID_IRQn_PRIORITY_SUBGROUP					0x2
#define HW_SPI1_GPIO_ID_IRQn_PRIORITY_PREEMPTION				0x2

#define HW_SPI1_DMA_SOURCE_ADDR									SPI6->DR
#define HW_SPI1_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_SPI1_DMA_RX_STREAM               					DMA2_Stream6
#define HW_SPI1_DMA_RX_CHANNEL              					DMA_Channel_1
#define HW_SPI1_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI1_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_SPI1_DMA_RX_STREAM_IRQn           					DMA2_Stream6_IRQn
#define HW_SPI1_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF6
#define HW_SPI1_DMA_RX_STREAM_IRQnHANDLER    					DMA2_Stream6_IRQHandler
#define HW_SPI1_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

#define HW_SPI1_DMA_TX_STREAM               					DMA2_Stream5
#define HW_SPI1_DMA_TX_CHANNEL              					DMA_Channel_1
#define HW_SPI1_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI1_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_SPI1_DMA_TX_STREAM_IRQn           					DMA2_Stream5_IRQn
#define HW_SPI1_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF5
#define HW_SPI1_DMA_TX_STREAM_IRQnHANDLER    					DMA2_Stream5_IRQHandler
#define HW_SPI1_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

/**
 * HW SPI2 peripherals register and I/O ports assignments
 */
#define HW_SPI2													SPI3
#define HW_SPI2_RCC_CLK											RCC_APB1Periph_SPI3
#define HW_SPI2_RCC_CLK_CMD_INIT								RCC_APB1PeriphClockCmd
#define HW_SPI2_IRQn											SPI3_IRQn
#define HW_SPI2_IRQnHANDLER										SPI3_IRQHandler
#define HW_SPI2_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_1
#define HW_SPI2_NVIC_PREEMPTION_PRIORITY						0x2
#define HW_SPI2_NVIC_SUB_PRIORITY								0x2

#define HW_SPI2_GPIO_PORT										GPIOC
#define HW_SPI2_GPIO_AF											GPIO_AF_SPI3
#define HW_SPI2_GPIO_MOSI_PIN									GPIO_Pin_12
#define HW_SPI2_GPIO_MOSI_SOURCE_PIN							GPIO_PinSource12
#define HW_SPI2_GPIO_MISO_PIN									GPIO_Pin_11
#define HW_SPI2_GPIO_MISO_SOURCE_PIN							GPIO_PinSource11
#define HW_SPI2_GPIO_CLK_PIN									GPIO_Pin_10
#define HW_SPI2_GPIO_CLK_SOURCE_PIN								GPIO_PinSource10

#define HW_SPI2_GPIO_PORT_ID									GPIOA
#define HW_SPI2_GPIO_ID_PIN										GPIO_Pin_15
#define HW_SPI2_GPIO_ID_PIN_SOURCE								EXTI_PinSource15
#define HW_SPI2_GPIO_ID_EXTI_LINE								EXTI_Line15

#define HW_SPI2_GPIO_ID_IRQn           							EXTI15_10_IRQn
#define HW_SPI2_GPIO_ID_IRQnHANDLER    							EXTI15_10_IRQHandler
#define HW_SPI2_GPIO_ID_EXTI_SOURCE								EXTI_PortSourceGPIOA
#define HW_SPI2_GPIO_ID_EXTI_TRIGGER_TYPE						EXTI_Trigger_Rising
#define HW_SPI2_GPIO_ID_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_SPI2_GPIO_ID_IRQn_PRIORITY_SUBGROUP					0x2
#define HW_SPI2_GPIO_ID_IRQn_PRIORITY_PREEMPTION				0x2

#define HW_SPI2_DMA_SOURCE_ADDR									SPI3->DR
#define HW_SPI2_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_SPI2_DMA_RX_STREAM               					DMA1_Stream0
#define HW_SPI2_DMA_RX_CHANNEL              					DMA_Channel_0
#define HW_SPI2_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI2_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA1
#define HW_SPI2_DMA_RX_STREAM_IRQn           					DMA1_Stream0_IRQn
#define HW_SPI2_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF0
#define HW_SPI2_DMA_RX_STREAM_IRQnHANDLER    					DMA1_Stream0_IRQHandler
#define HW_SPI2_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

#define HW_SPI2_DMA_TX_STREAM               					DMA1_Stream5
#define HW_SPI2_DMA_TX_CHANNEL              					DMA_Channel_0
#define HW_SPI2_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI2_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA1
#define HW_SPI2_DMA_TX_STREAM_IRQn           					DMA1_Stream5_IRQn
#define HW_SPI2_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF5
#define HW_SPI2_DMA_TX_STREAM_IRQnHANDLER    					DMA1_Stream5_IRQHandler
#define HW_SPI2_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

/**
 * HW SPI3 peripherals register and I/O ports assignments
 */
#define HW_SPI3													SPI2
#define HW_SPI3_RCC_CLK											RCC_APB1Periph_SPI2
#define HW_SPI3_RCC_CLK_CMD_INIT								RCC_APB1PeriphClockCmd
#define HW_SPI3_IRQn											SPI2_IRQn
#define HW_SPI3_IRQnHANDLER										SPI2_IRQHandler
#define HW_SPI3_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_2
#define HW_SPI3_NVIC_PREEMPTION_PRIORITY						0x3
#define HW_SPI3_NVIC_SUB_PRIORITY								0x3

#define HW_SPI3_GPIO_PORT										GPIOB
#define HW_SPI3_GPIO_AF											GPIO_AF_SPI2
#define HW_SPI3_GPIO_MOSI_PIN									GPIO_Pin_15
#define HW_SPI3_GPIO_MOSI_SOURCE_PIN							GPIO_PinSource15
#define HW_SPI3_GPIO_MISO_PIN									GPIO_Pin_14
#define HW_SPI3_GPIO_MISO_SOURCE_PIN							GPIO_PinSource14
#define HW_SPI3_GPIO_CLK_PIN									GPIO_Pin_13
#define HW_SPI3_GPIO_CLK_SOURCE_PIN								GPIO_PinSource13

#define HW_SPI3_GPIO_PORT_ID									GPIOB
#define HW_SPI3_GPIO_ID_PIN										GPIO_Pin_12
#define HW_SPI3_GPIO_ID_PIN_SOURCE								EXTI_PinSource12
#define HW_SPI3_GPIO_ID_EXTI_LINE								EXTI_Line12

#define HW_SPI3_GPIO_ID_IRQn           							EXTI15_10_IRQn
#define HW_SPI3_GPIO_ID_IRQnHANDLER    							EXTI15_10_IRQHandler
#define HW_SPI3_GPIO_ID_EXTI_SOURCE								EXTI_PortSourceGPIOB
#define HW_SPI3_GPIO_ID_EXTI_TRIGGER_TYPE						EXTI_Trigger_Rising
#define HW_SPI3_GPIO_ID_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_SPI3_GPIO_ID_IRQn_PRIORITY_SUBGROUP					0x2
#define HW_SPI3_GPIO_ID_IRQn_PRIORITY_PREEMPTION				0x2

#define HW_SPI3_DMA_SOURCE_ADDR									SPI2->DR
#define HW_SPI3_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_SPI3_DMA_RX_STREAM               					DMA1_Stream3
#define HW_SPI3_DMA_RX_CHANNEL              					DMA_Channel_0
#define HW_SPI3_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI3_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA1
#define HW_SPI3_DMA_RX_STREAM_IRQn           					DMA1_Stream3_IRQn
#define HW_SPI3_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF3
#define HW_SPI3_DMA_RX_STREAM_IRQnHANDLER    					DMA1_Stream3_IRQHandler
#define HW_SPI3_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

#define HW_SPI3_DMA_TX_STREAM               					DMA1_Stream4
#define HW_SPI3_DMA_TX_CHANNEL              					DMA_Channel_0
#define HW_SPI3_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI3_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA1
#define HW_SPI3_DMA_TX_STREAM_IRQn           					DMA1_Stream4_IRQn
#define HW_SPI3_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF4
#define HW_SPI3_DMA_TX_STREAM_IRQnHANDLER    					DMA1_Stream4_IRQHandler
#define HW_SPI3_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

/**
 * HW SPI4 peripherals register and I/O ports assignments
 */
#define HW_SPI4													SPI1
#define HW_SPI4_RCC_CLK											RCC_APB2Periph_SPI1
#define HW_SPI4_RCC_CLK_CMD_INIT								RCC_APB2PeriphClockCmd
#define HW_SPI4_IRQn											SPI1_IRQn
#define HW_SPI4_IRQnHANDLER										SPI1_IRQHandler
#define HW_SPI4_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_2
#define HW_SPI4_NVIC_PREEMPTION_PRIORITY						0x4
#define HW_SPI4_NVIC_SUB_PRIORITY								0x4

#define HW_SPI4_GPIO_PORT										GPIOA
#define HW_SPI4_GPIO_AF											GPIO_AF_SPI1
#define HW_SPI4_GPIO_MOSI_PIN									GPIO_Pin_7
#define HW_SPI4_GPIO_MOSI_SOURCE_PIN							GPIO_PinSource7
#define HW_SPI4_GPIO_MISO_PIN									GPIO_Pin_6
#define HW_SPI4_GPIO_MISO_SOURCE_PIN							GPIO_PinSource6
#define HW_SPI4_GPIO_CLK_PIN									GPIO_Pin_5
#define HW_SPI4_GPIO_CLK_SOURCE_PIN								GPIO_PinSource5

#define HW_SPI4_GPIO_PORT_ID									GPIOA
#define HW_SPI4_GPIO_ID_PIN										GPIO_Pin_4
#define HW_SPI4_GPIO_ID_PIN_SOURCE								EXTI_PinSource4
#define HW_SPI4_GPIO_ID_EXTI_LINE								EXTI_Line4

#define HW_SPI4_GPIO_ID_IRQn           							EXTI4_IRQn
#define HW_SPI4_GPIO_ID_IRQnHANDLER    							EXTI4_IRQHandler
#define HW_SPI4_GPIO_ID_EXTI_SOURCE								EXTI_PortSourceGPIOA
#define HW_SPI4_GPIO_ID_EXTI_TRIGGER_TYPE						EXTI_Trigger_Rising
#define HW_SPI4_GPIO_ID_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_SPI4_GPIO_ID_IRQn_PRIORITY_SUBGROUP					0x2
#define HW_SPI4_GPIO_ID_IRQn_PRIORITY_PREEMPTION				0x2

#define HW_SPI4_DMA_SOURCE_ADDR									SPI1->DR
#define HW_SPI4_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_SPI4_DMA_RX_STREAM               					DMA2_Stream0
#define HW_SPI4_DMA_RX_CHANNEL              					DMA_Channel_3
#define HW_SPI4_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI4_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_SPI4_DMA_RX_STREAM_IRQn           					DMA2_Stream0_IRQn
#define HW_SPI4_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF0
#define HW_SPI4_DMA_RX_STREAM_IRQnHANDLER    					DMA2_Stream0_IRQHandler
#define HW_SPI4_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

/* SPI4 TX is not available - only standard interrupt! */
//#define HW_SPI4_DMA_TX_STREAM               					DMA1_Stream4
//#define HW_SPI4_DMA_TX_CHANNEL              					DMA_Channel_0
//#define HW_SPI4_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
//#define HW_SPI4_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA1
//#define HW_SPI4_DMA_TX_STREAM_IRQn           					DMA1_Stream4_IRQn
//#define HW_SPI4_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF4
//#define HW_SPI4_DMA_TX_STREAM_IRQnHANDLER    					DMA1_Stream4_IRQHandler
//#define HW_SPI4_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

/**
 * HW SPI5 peripherals register and I/O ports assignments
 */
#define HW_SPI5													SPI5
#define HW_SPI5_RCC_CLK											RCC_APB2Periph_SPI5
#define HW_SPI5_RCC_CLK_CMD_INIT								RCC_APB2PeriphClockCmd
#define HW_SPI5_IRQn											SPI5_IRQn
#define HW_SPI5_IRQnHANDLER										SPI5_IRQHandler
#define HW_SPI5_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_2
#define HW_SPI5_NVIC_PREEMPTION_PRIORITY						0x5
#define HW_SPI5_NVIC_SUB_PRIORITY								0x5

#define HW_SPI5_GPIO_PORT										GPIOF
#define HW_SPI5_GPIO_AF											GPIO_AF_SPI5
#define HW_SPI5_GPIO_MOSI_PIN									GPIO_Pin_9
#define HW_SPI5_GPIO_MOSI_SOURCE_PIN							GPIO_PinSource9
#define HW_SPI5_GPIO_MISO_PIN									GPIO_Pin_8
#define HW_SPI5_GPIO_MISO_SOURCE_PIN							GPIO_PinSource8
#define HW_SPI5_GPIO_CLK_PIN									GPIO_Pin_7
#define HW_SPI5_GPIO_CLK_SOURCE_PIN								GPIO_PinSource7

#define HW_SPI5_GPIO_PORT_ID									GPIOF
#define HW_SPI5_GPIO_ID_PIN										GPIO_Pin_6
#define HW_SPI5_GPIO_ID_PIN_SOURCE								EXTI_PinSource6
#define HW_SPI5_GPIO_ID_EXTI_LINE								EXTI_Line6

#define HW_SPI5_GPIO_ID_IRQn           							EXTI9_5_IRQn
#define HW_SPI5_GPIO_ID_IRQnHANDLER    							EXTI9_5_IRQHandler
#define HW_SPI5_GPIO_ID_EXTI_SOURCE								EXTI_PortSourceGPIOF
#define HW_SPI5_GPIO_ID_EXTI_TRIGGER_TYPE						EXTI_Trigger_Rising
#define HW_SPI5_GPIO_ID_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_SPI5_GPIO_ID_IRQn_PRIORITY_SUBGROUP					0x2
#define HW_SPI5_GPIO_ID_IRQn_PRIORITY_PREEMPTION				0x2

#define HW_SPI5_DMA_SOURCE_ADDR									SPI5->DR
#define HW_SPI5_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_SPI5_DMA_RX_STREAM               					DMA2_Stream3
#define HW_SPI5_DMA_RX_CHANNEL              					DMA_Channel_2
#define HW_SPI5_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI5_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_SPI5_DMA_RX_STREAM_IRQn           					DMA2_Stream3_IRQn
#define HW_SPI5_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF3
#define HW_SPI5_DMA_RX_STREAM_IRQnHANDLER    					DMA2_Stream3_IRQHandler
#define HW_SPI5_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

#define HW_SPI5_DMA_TX_STREAM               					DMA2_Stream4
#define HW_SPI5_DMA_TX_CHANNEL              					DMA_Channel_2
#define HW_SPI5_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_SPI5_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_SPI5_DMA_TX_STREAM_IRQn           					DMA2_Stream4_IRQn
#define HW_SPI5_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF4
#define HW_SPI5_DMA_TX_STREAM_IRQnHANDLER    					DMA2_Stream4_IRQHandler
#define HW_SPI5_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1


/*******************************************************************************
  * SDRAM parameters
  ******************************************************************************/
#define HW_SDRAM_START_ADR             							((uint32_t)0xC0000000)
#define HW_SDRAM_BANK_ADDR     									((uint32_t)0xC0000000)
#define HW_SDRAM_BASE 											0xC0000000
#define HW_SDRAM_SIZE 											0x1000000

#define HW_SDRAM_MEMORY_WIDTH    								FMC_SDMemory_Width_16b
#define HW_SDRAM_CAS_LATENCY    								FMC_CAS_Latency_2
#define HW_SDCLOCK_PERIOD    									FMC_SDClock_Period_2
#define HW_SDRAM_READBURST    									FMC_Read_Burst_Enable

/*******************************************************************************
  * NOR Flash parameters
  ******************************************************************************/
#define HW_NOR_START_ADR             							((uint32_t)0xC0000000)
#define HW_NOR_BANK_ADDR        								((uint32_t)0x60000000)
#define HW_NOR_BASE 											0xC0000000
#define HW_NOR_SIZE												0x1FFFFF

/*******************************************************************************
  * TFT LCD parameters
  ******************************************************************************/
/**
 * TFT LCD parameters
 */
#define HW_LCD_PIXEL_WIDTH  									800
#define HW_LCD_PIXEL_HEIGHT 									480
#define HW_LCD_PIXEL_BYTE_WIDTH									0x2
#define HW_LCD_PIXEL_FORMAT										LTDC_Pixelformat_RGB565
#define HW_LCD_PIXEL_TOTAL_NUM									(uint32_t)((HW_LCD_PIXEL_WIDTH)*(HW_LCD_PIXEL_HEIGHT))

#define HW_LCD_SCREEN_DATA_BUFF_SIZE							( (uint32_t)((HW_LCD_PIXEL_WIDTH)*(HW_LCD_PIXEL_HEIGHT)*\
																			(HW_LCD_PIXEL_BYTE_WIDTH)) )

#define HW_LCD_FRAME_BUFFER_SIZE								(uint32_t)((HW_LCD_PIXEL_WIDTH)*(HW_LCD_PIXEL_HEIGHT))

#define HW_LCD_BACKGROUND_COLOUR_BLACK							0x0000

#define HW_LCD_LAYER1_BUFFER_ADDR								( (uint32_t)HW_SDRAM_BASE )
#define HW_LCD_LAYER2_BUFFER_OFFSET								( (uint32_t)((HW_LCD_PIXEL_WIDTH)*(HW_LCD_PIXEL_HEIGHT)*\
																		(HW_LCD_PIXEL_BYTE_WIDTH)) )
#define HW_LCD_LAYER2_BUFFER_ADDR								( (uint32_t)(HW_SDRAM_BASE + HW_LCD_LAYER2_BUFFER_OFFSET) )

#define HW_LCD_LAYER1											LTDC_Layer1
#define HW_LCD_LAYER2											LTDC_Layer2

#define HW_LCD_MAIN_LAYER										HW_LCD_LAYER1
#define HW_LCD_SPECIAL_LAYER									HW_LCD_LAYER2

#define HW_LCD_TRANSPARENT_FULL_ENABLED							(0x00)
#define HW_LCD_TRANSPARENT_DISABLED								(0xFF)


/**
 * TFT LCD I/O ports assignments
 */
#define HW_LCD_GPIO_ENABLE_PORT									GPIOI
#define HW_LCD_GPIO_ENABLE_PIN									GPIO_Pin_5

#define HW_LCD_GPIO_BACKLIGHT_EN_PORT							GPIOE
#define HW_LCD_GPIO_BACKLIGHT_EN_PIN							GPIO_Pin_2

/**
 * TFT LCD Interrupt based on the DMA2D
 */
#define HW_LCD_DMA2D_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_LCD_DMA2D_CLOCK         								RCC_AHB1Periph_DMA2D
#define HW_LCD_DMA2D_IRQn           							DMA2D_IRQn
#define HW_LCD_DMA2D_IRQn_TYPE									DMA2D_IT_TC
#define HW_LCD_DMA2D_IT_INT_FLAG              					DMA2D_FLAG_TC
#define HW_LCD_DMA2D_IRQnHANDLER    							DMA2D_IRQHandler
#define HW_LCD_DMA2D_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_2
#define HW_LCD_DMA2D_NVIC_PREEMPTION_PRIORITY					0x2
#define HW_LCD_DMA2D_NVIC_SUB_PRIORITY							0x2

/**
 * TFT LCD interrupt configuration - for IMR mode of the LTDC controller
 */
#define HW_LCD_LTDC_IRQn           								LTDC_IRQn
#define HW_LCD_LTDC_IRQn_TYPE									LTDC_IT_RR
#define HW_LCD_LTDC_IT_INT_FLAG              					LTDC_FLAG_RR
#define HW_LCD_LTDC_IRQnHANDLER    								LTDC_IRQHandler
#define HW_LCD_LTDC_NVIC_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_LCD_LTDC_NVIC_PREEMPTION_PRIORITY					0x5
#define HW_LCD_LTDC_NVIC_SUB_PRIORITY							0x5

/*******************************************************************************
  * LED diodes parameters
  ******************************************************************************/
#define HW_LEDs_GPIO_PORT										GPIOB
#define HW_LEDs_GPIO_RED_PIN									GPIO_Pin_6
#define HW_LEDs_GPIO_YELLOW_PIN									GPIO_Pin_5
#define HW_LEDs_GPIO_GREEN_PIN									GPIO_Pin_4


/*******************************************************************************
  * USART parameters
  ******************************************************************************/
#define HW_COM_MODE_INTERRUPT									(0x00)
#define HW_COM_MODE_DMA											(0x01)

#define HW_COM1_TX_IRQn_ENABLE									(0x00)
#define HW_COM1_RX_IRQn_ENABLE									(0x00)
#define HW_COM1_IDLE_IRQn_ENABLE								(0x01)
#define HW_COM1_ERROR_IRQn_ENABLE								(0x00)

#define HW_COM1_OPERATE_MODE									(HW_COM_MODE_DMA)

#define HW_COM1_SPEED											(uint32_t)(256000)
#define HW_COM1_TX_IT_MODE										USART_IT_TXE
#define HW_COM1_RX_IT_MODE										USART_IT_RXNE

#define HW_COM1													USART6
#define HW_COM1_RCC_CLK											RCC_APB2Periph_USART6
#define HW_COM1_RCC_CLK_CMD_INIT								RCC_APB2PeriphClockCmd
#define HW_COM1_IRQn											USART6_IRQn
#define HW_COM1_IRQnHANDLER										USART6_IRQHandler
#define HW_COM1_NVIC_PRIORITY_GROUP								NVIC_PriorityGroup_2
#define HW_COM1_NVIC_PREEMPTION_PRIORITY						0x5
#define HW_COM1_NVIC_SUB_PRIORITY								0x5

#define HW_COM1_GPIO_PORT										GPIOC
#define HW_COM1_GPIO_AF											GPIO_AF_USART6
#define HW_COM1_GPIO_RX_PIN										GPIO_Pin_7
#define HW_COM1_GPIO_RX_SOURCE_PIN								GPIO_PinSource7
#define HW_COM1_GPIO_TX_PIN										GPIO_Pin_6
#define HW_COM1_GPIO_TX_SOURCE_PIN								GPIO_PinSource6


#define HW_COM1_DMA_SOURCE_ADDR									(HW_COM1)->DR
#define HW_COM1_DMA_IRQn_TYPE									DMA_IT_TC

#define HW_COM1_DMA_RX_STREAM               					DMA2_Stream2
#define HW_COM1_DMA_RX_CHANNEL              					DMA_Channel_5
#define HW_COM1_DMA_RX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_COM1_DMA_RX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_COM1_DMA_RX_STREAM_IRQn           					DMA2_Stream2_IRQn
#define HW_COM1_DMA_RX_IT_INT_FLAG              				DMA_IT_TCIF2
#define HW_COM1_DMA_RX_STREAM_IRQnHANDLER    					DMA2_Stream2_IRQHandler
#define HW_COM1_DMA_RX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1

#define HW_COM1_DMA_TX_STREAM               					DMA2_Stream7
#define HW_COM1_DMA_TX_CHANNEL              					DMA_Channel_5
#define HW_COM1_DMA_TX_RCC_CLK_CMD_INIT							RCC_AHB1PeriphClockCmd
#define HW_COM1_DMA_TX_STREAM_CLOCK         					RCC_AHB1Periph_DMA2
#define HW_COM1_DMA_TX_STREAM_IRQn           					DMA2_Stream7_IRQn
#define HW_COM1_DMA_TX_IT_INT_FLAG              				DMA_IT_TCIF7
#define HW_COM1_DMA_TX_STREAM_IRQnHANDLER    					DMA2_Stream7_IRQHandler
#define HW_COM1_DMA_TX_NVIC_PRIORITY_GROUP						NVIC_PriorityGroup_1


/*******************************************************************************
  * User Buttons parameters
  ******************************************************************************/
/*------------------Switch 0---------------------*/
#define HW_SW0_GPIO_PORT										GPIOI
#define HW_SW0_GPIO_PIN											GPIO_Pin_0
#define HW_SW0_EXTI_SOURCE										EXTI_PortSourceGPIOI
#define HW_SW0_EXTI_PIN_SOURCE									EXTI_PinSource0
#define HW_SW0_EXTI_LINE										EXTI_Line0
#define HW_SW0_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 1---------------------*/
#define HW_SW1_GPIO_PORT										GPIOC
#define HW_SW1_GPIO_PIN											GPIO_Pin_8
#define HW_SW1_EXTI_SOURCE										EXTI_PortSourceGPIOC
#define HW_SW1_EXTI_PIN_SOURCE									EXTI_PinSource8
#define HW_SW1_EXTI_LINE										EXTI_Line8
#define HW_SW1_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 2---------------------*/
#define HW_SW2_GPIO_PORT										GPIOI
#define HW_SW2_GPIO_PIN											GPIO_Pin_2
#define HW_SW2_EXTI_SOURCE										EXTI_PortSourceGPIOI
#define HW_SW2_EXTI_PIN_SOURCE									EXTI_PinSource2
#define HW_SW2_EXTI_LINE										EXTI_Line2
#define HW_SW2_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 3---------------------*/
#define HW_SW3_GPIO_PORT										GPIOI
#define HW_SW3_GPIO_PIN											GPIO_Pin_3
#define HW_SW3_EXTI_SOURCE										EXTI_PortSourceGPIOI
#define HW_SW3_EXTI_PIN_SOURCE									EXTI_PinSource3
#define HW_SW3_EXTI_LINE										EXTI_Line3
#define HW_SW3_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 4---------------------*/
#define HW_SW4_GPIO_PORT										GPIOH
#define HW_SW4_GPIO_PIN											GPIO_Pin_15
#define HW_SW4_EXTI_SOURCE										EXTI_PortSourceGPIOH
#define HW_SW4_EXTI_PIN_SOURCE									EXTI_PinSource15
#define HW_SW4_EXTI_LINE										EXTI_Line15
#define HW_SW4_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 5---------------------*/
#define HW_SW5_GPIO_PORT										GPIOI
#define HW_SW5_GPIO_PIN											GPIO_Pin_1
#define HW_SW5_EXTI_SOURCE										EXTI_PortSourceGPIOI
#define HW_SW5_EXTI_PIN_SOURCE									EXTI_PinSource1
#define HW_SW5_EXTI_LINE										EXTI_Line1
#define HW_SW5_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 6---------------------*/
#define HW_SW6_GPIO_PORT										GPIOH
#define HW_SW6_GPIO_PIN											GPIO_Pin_14
#define HW_SW6_EXTI_SOURCE										EXTI_PortSourceGPIOH
#define HW_SW6_EXTI_PIN_SOURCE									EXTI_PinSource14
#define HW_SW6_EXTI_LINE										EXTI_Line14
#define HW_SW6_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising

/*------------------Switch 7---------------------*/
#define HW_SW7_GPIO_PORT										GPIOC
#define HW_SW7_GPIO_PIN											GPIO_Pin_9
#define HW_SW7_EXTI_SOURCE										EXTI_PortSourceGPIOC
#define HW_SW7_EXTI_PIN_SOURCE									EXTI_PinSource9
#define HW_SW7_EXTI_LINE										EXTI_Line9
#define HW_SW7_EXTI_TRIGGER_TYPE								EXTI_Trigger_Rising


/*******************************************************************************
  * External Interrupt
  ******************************************************************************/
#define HW_EXTI0_IRQn											EXTI0_IRQn
#define HW_EXTI0_IRQnHANDLER									EXTI0_IRQHandler
#define HW_EXTI0_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI0_IRQn_PRIORITY_SUBGROUP							0xA
#define HW_EXTI0_IRQn_PRIORITY_PREEMPTION						0x6

#define HW_EXTI1_IRQn											EXTI1_IRQn
#define HW_EXTI1_IRQnHANDLER									EXTI1_IRQHandler
#define HW_EXTI1_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI1_IRQn_PRIORITY_SUBGROUP							0xA
#define HW_EXTI1_IRQn_PRIORITY_PREEMPTION						0x5

#define HW_EXTI2_IRQn											EXTI2_IRQn
#define HW_EXTI2_IRQnHANDLER									EXTI2_IRQHandler
#define HW_EXTI2_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI2_IRQn_PRIORITY_SUBGROUP							0xA
#define HW_EXTI2_IRQn_PRIORITY_PREEMPTION						0x4

#define HW_EXTI3_IRQn											EXTI3_IRQn
#define HW_EXTI3_IRQnHANDLER									EXTI3_IRQHandler
#define HW_EXTI3_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI3_IRQn_PRIORITY_SUBGROUP							0xA
#define HW_EXTI3_IRQn_PRIORITY_PREEMPTION						0x3

#define HW_EXTI4_IRQn											EXTI4_IRQn
#define HW_EXTI4_IRQnHANDLER									EXTI4_IRQHandler
#define HW_EXTI4_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI4_IRQn_PRIORITY_SUBGROUP							0xA
#define HW_EXTI4_IRQn_PRIORITY_PREEMPTION						0x2

#define HW_EXTI9_5_IRQn											EXTI9_5_IRQn
#define HW_EXTI9_5_IRQnHANDLER									EXTI9_5_IRQHandler
#define HW_EXTI9_5_IRQn_PRIORITY_GROUP							NVIC_PriorityGroup_4
#define HW_EXTI9_5_IRQn_PRIORITY_SUBGROUP						0xA
#define HW_EXTI9_5_IRQn_PRIORITY_PREEMPTION						0x1

#define HW_EXTI15_10_IRQn										EXTI15_10_IRQn
#define HW_EXTI15_10_IRQnHANDLER								EXTI15_10_IRQHandler
#define HW_EXTI15_10_IRQn_PRIORITY_GROUP						NVIC_PriorityGroup_4
#define HW_EXTI15_10_IRQn_PRIORITY_SUBGROUP						0xA
#define HW_EXTI15_10_IRQn_PRIORITY_PREEMPTION					0x0

/*******************************************************************************
  * RNG Random Generator
  ******************************************************************************/
#define HW_HASH_RNG_IRQn										HASH_RNG_IRQn
#define HW_HASH_RNG_IRQnHANDLER									HASH_RNG_IRQHandler
#define HW_RNG_IRQn_PRIORITY_GROUP								NVIC_PriorityGroup_4
#define HW_RNG_IRQn_PRIORITY_SUBGROUP							0x6
#define HW_RNG_IRQn_PRIORITY_PREEMPTION							0x6


#define HW_WATCHDOG_WINDOWS_USE									0x00

#endif /* HWDRIVERS_INC_HW_SYSTEMCONFIG_H_ */