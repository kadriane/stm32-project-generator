/**
  ******************************************************************************
  * @file    HW_IRQnHandlers.h
  * @brief   This file contains the headers of the interrupt handlers.
  ******************************************************************************
**/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_IRQNHANDLER_H
#define __HW_IRQNHANDLER_H

/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void SysTick_Handler(void);
void PVD_IRQHandler(void);
void FLASH_IRQHandler(void);
void RCC_IRQHandler(void);

#endif
/*******************************END OF FILE************************************/
