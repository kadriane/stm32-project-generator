'''
CLI helper for STM32 Project Generator
'''
from stm32projgenerator import stm32projgenerator

if __name__ == '__main__':
    stm32projgenerator.main()
