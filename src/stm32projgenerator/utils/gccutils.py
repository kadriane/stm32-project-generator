'''
GCC compiler specific utilities.
'''
import logging
import subprocess

from pathlib import Path

from stm32projgenerator.utils import utils

LOGGER_NAME = "__gccutils__"

_COMPILERS = [
    'gcc',
    'g++',
    'arm-none-eabi-gcc'
]

def precompile_file(fpath: Path):
    '''
    Precompile C file using GNU precompiler.

    This function checks for the availability of GNU compilers (`gcc`, `g++`, `arm-none-eabi-gcc`)
    and uses one of them to preprocess the specified  C source file. The precompiled output
    is written back to the original file location.

    GCC Flags:
        * - **-E**: Enable precompiler.
        * - **-P**: Do not generate #line directives.
        * - **-C**: Do not ignore comments.
        * - **-x c**: Interpret files as C files.

    :param fpath: The path to the C file that needs to be precompiled.
    :type fpath: Path
    
    :raises EnvironmentError: If the GCC precompilation fails, an error is logged and raised.

    :example:
    
        >>> precompile_file(Path('example.c'))
        # This will precompile 'example.c' using GCC and overwrite it with the precompiled output.
    '''
    logger = logging.getLogger(LOGGER_NAME)
    logger.debug(f'# GCC Precompiler enabled for file "{fpath.name}" in location "{fpath}".')

    compiler = next((c for c in _COMPILERS if utils.check_executables(c)), None)
    if not compiler:
        _errmsg = f'Cannot precompile files - GNU compilers not found (expected={_COMPILERS}'
        logger.error(_errmsg)
        raise EnvironmentError(_errmsg)

    try:
        cmd = f'cd {fpath.parent};{compiler} -E -P -C -x c {fpath.name}'
        logger.debug(f'# Input GCC subprocess commands: "{cmd}".')

        p = subprocess.run(cmd, stdout=subprocess.PIPE, check=True, shell=True)
        data = p.stdout.decode('utf-8')
        fpath.write_text(data)

    except Exception as ex:
        logger.error(f'# GCC precompiler failed for file "{fpath}".\n# Error msg:"{ex}".')
        raise EnvironmentError(ex) from ex

    logger.debug(f'# The file "{fpath.name}" precompiled. Output data has been written to file.')


def test_compilation(project_path: Path | str):
    '''
    Test compilation for a generated projects using GCC.

    This function attempts to compile a project located at the specified path using 
    the `make` command. It runs `make clean` followed by `make all` to ensure that 
    the project is built from a clean state. If the compilation fails, an error is logged 
    and an exception is raised.

    :param project_path: The path to the project directory that contains the Makefile.
                         This can be provided as a string or a Path object.
    :type project_path: Path | str
    
    :raises EnvironmentError: If the compilation process fails, an error is logged 
                             and raised with details about the failure.

    :example:
    
        >>> test_compilation('./my_project')
        # This will test compilation for the project located in './my_project'.
    '''
    logger = logging.getLogger(LOGGER_NAME)
    logger.info('# Test compilation of created project in progress...')

    try:
        path = project_path if isinstance(project_path, Path) else Path(project_path)
        cmds = f'cd {path}; make clean; make all'
        subprocess.run(cmds,
                       shell=True,
                       check=True,
                       encoding='utf-8',
                       capture_output=True,
                       timeout=10)

    except subprocess.CalledProcessError as ex:
        logger.error('# Test compilation subprocess failed for created project during starting.')
        logger.error(f'# Error message: {ex.stderr}')
        raise EnvironmentError(ex) from ex

    logger.info('# Test compilation finished successfully.')
