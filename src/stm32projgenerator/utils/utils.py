'''
Miscelaneous general purpose utilities for STM32 Project Generator.
'''
import re
import logging
import shutil

from pathlib import Path

import ruamel.yaml

LOGGER_NAME = 'utils'


def find_substring(data: str, patterns: list) -> str | None:
    '''
    Find all regex patterns in provided input string.

    This function iterates over a list of regex patterns and returns the first match found
    in the input string. If no matches are found, it returns None.

    :param data: The input string in which to search for substrings.
    :type data: str
    :param patterns: A list of regex patterns to search for in the input string.
    :type patterns: list
    :return: The first matching substring if found; otherwise, None.
    :rtype: str | None

    :example:

        >>> result = find_substring("Hello, world!", [r"Hello", r"world"])
        >>> print(result)
        'Hello'

        >>> result = find_substring("Hello, world!", [r"Python"])
        >>> print(result)
        None
    '''
    for pattern in patterns:
        result = re.findall(pattern, data)
        if result:
            return result[0]
    return None


def file_search_all(file_path: Path, substrings: list) -> bool:
    '''
    Check if a file contains all strings from provided list

    This function reads the contents of a specified file and checks if all specified
    substrings are present within that content. It returns True if all substrings are found;
    otherwise, it returns False.

    :param file_path: The path to the file to be searched.
    :type file_path: Path
    :param substrings: A list of substrings to search for in the file content.
    :type substrings: list
    :return: True if all substrings are found in the file; otherwise, False.
    :rtype: bool

    :example:

        >>> found = file_search_all(Path('example.txt'), ['Hello', 'world'])
        >>> print(found)
        True

        >>> found = file_search_all(Path('example.txt'), ['Python', 'Java'])
        >>> print(found)
        False
    '''
    data = ''
    with open(str(file_path), encoding='utf-8') as ff:
        data = ff.read()

    return all(str(substr) in data for substr in substrings)


def check_executables(execs: list | str) -> bool:
    '''
    Check if the specified executables are installed on the system.

    This function verifies the presence of one or more executables in the system's PATH.
    It logs debug messages during the checking process and returns a boolean indicating
    whether all specified executables were found.

    Parameters
    ----------
    execs : list | str
        A list of executable names or a single executable name as a string to check for their
        presence. If an empty list or None is provided, the function will abort and return False.

    Returns
    -------
    bool
        Returns True if all specified executables are found; otherwise, returns False.

    Examples
    --------
    >>> check_executables(['python', 'git'])
    True

    >>> check_executables('gcc')
    False

    Notes
    -----
    - The function uses `shutil.which` to determine if each executable is available
      in the system's PATH.
    - It logs detailed messages at the debug level which can be useful for troubleshooting.

    Raises
    ------
    ValueError
        If `execs` is not a list or string.

    See Also
    --------
    shutil.which : A utility function to check for executable availability.

    '''
    logger = logging.getLogger(LOGGER_NAME)
    logger.debug('Checking if executables are installed in progress...')

    if not execs:
        logger.debug('Executables not provided - abort.')
        return False

    if isinstance(execs, str):
        execs = [execs]

    for exe in execs:
        if not shutil.which(exe):
            logger.error(f'Executable "{exe}" not found - abort.')
            return False
        logger.debug(f'Executable "{exe}" - OK.')

    return True


def load_yaml_file(fpath: str) -> dict:
    '''
    Load YAML file into dictionary using ruamel.yaml.

    This function reads a YAML file from the specified path and loads its contents
    into a Python dictionary. If the file does not exist, an error is logged and
    a RuntimeError is raised.

    :param fpath: The path to the YAML file to be loaded.
    :type fpath: str
    :return: A dictionary containing the data loaded from the YAML file.
    :rtype: dict

    :raises RuntimeError: If the specified file does not exist.

    :example:

        >>> config = load_yaml_file('config.yaml')
        # This will load the contents of 'config.yaml' into a dictionary.

        >>> config = load_yaml_file('nonexistent.yaml')
        # Raises RuntimeError as the file does not exist.
    '''
    logger = logging.getLogger(LOGGER_NAME)

    if not Path(fpath).exists():
        errmsg = f'# The file "{fpath}" not exist.'
        logger.error(errmsg)
        raise RuntimeError(errmsg)

    with open(fpath, 'r', encoding='utf-8') as ff:
        yaml = ruamel.yaml.YAML(typ='safe', pure=True)
        config = yaml.load(ff)
    return config
