'''
Files and folders miscelaneous utilities
'''
import logging

from pathlib import Path
from string import Template

from stm32projgenerator.generators.defs import FilesConfig
from stm32projgenerator.utils import gccutils

logger = logging.getLogger(__name__)


def customize_template_file(destfile: Path, srcfile: Path, params: dict | None):
    '''
    Customize destination file (destfile), based on the template (srcfile) with custom paramerers
    (params).

    This function customizes the destination file (`destfile`) based on the template 
    file (`srcfile`) using the provided custom parameters (`params`). If the `params` 
    dictionary is empty, the template is copied as-is to the destination file.

    :param destfile: The path to the destination file where the customized content will be written.
    :type destfile: Path
    :param srcfile: The path to the source template file to be customized.
    :type srcfile: Path
    :param params: A dictionary of parameters to customize the template. If None or empty, 
                   the template is copied directly.
    :type params: dict | None
    :raises FileNotFoundError: If the source template file does not exist.
    :raises KeyError: If a required parameter for customization is missing.
    
    :example:
    
        >>> from pathlib import Path
        >>> customize_template_file(Path('dest.txt'), Path('template.txt'), {'name': 'John'})
    
    The above example customizes `template.txt` and writes the result to `dest.txt`,
    replacing `{name}` in the template with "John".
    '''
    if not srcfile.exists():
        logger.error(f'Template file "{srcfile}" not exists!')
        raise FileNotFoundError(f'Template file "{srcfile}" not exists!')

    logger.debug(f'# Customizing and reading source file "{srcfile.name}" with params "{params}"'
                 ' in progress...')

    filename = destfile.name
    data = srcfile.read_text(encoding='cp1252')

    # Customizing template file params with String.Template
    if params:
        try:
            data = Template(data).substitute(params)
        except KeyError as key:
            errmsg = f'# Customizing file "{filename}" failed. Missing template parameter: {key}.'
            logger.error(errmsg)
            raise KeyError(errmsg) from key
        logger.debug(f'# Customizing file "{filename}" done.')

    destfile.write_text(data)
    logger.debug(f'# Writting red and customized data to file "{filename}" -> done.')


def generate_folders(folders: dict | Path, destpath: Path = Path('./out')):
    '''
    Create multiple folders in the output directory.

    This function creates folders based on the provided dictionary or single path. If a folder 
    already exists, it will skip the creation of that folder. The default output directory is 
    './out', but this can be customized by providing a different path.

    :param folders: A dictionary mapping folder names to their paths or a single path as a 
                    string or Path object. If a dictionary is provided, the keys represent 
                    the folder names and the values represent the paths relative to `destpath`.
    :type folders: dict | Path
    :param destpath: The base path where the folders will be created. Default is './out'.
    :type destpath: Path
    
    :example:
    
        >>> generate_folders({'folder1': 'subfolder1', 'folder2': 'subfolder2'})
        # This will create './out/subfolder1' and './out/subfolder2'.
        
        >>> generate_folders(Path('single_folder'))
        # This will create './out/single_folder'.

    :raises Exception: Any exceptions raised during folder creation (e.g., permission issues) 
                      will be logged but not raised.
    '''
    logger.info('Creating project folders in progress...')

    folders_dict = folders if isinstance(folders, dict) else {str(folders): folders}
    for folder, path in folders_dict.items():
        folder_path = Path(f'{destpath}/{path}')
        if folder_path.is_dir():
            logger.debug(f'# Folder "{folder}" in location "{path}" already exists.')
            continue

        folder_path.mkdir(parents=True, exist_ok=True)  # exceptions disabled
        logger.debug(f'# Folder "{folder}" created.')

    logger.info('# Creating project folders -> done.')


def generate_files(params: FilesConfig, project_path: Path, force: bool = False):
    '''
    Generate and customize files in the output project folder.

    This function searches for source files matching a specified pattern in the source path,
    customizes them according to the provided configuration, and generates them in the 
    destination project folder. If a file already exists and `force` is set to False, 
    it will skip overwriting that file.

    :param params: Configuration parameters for file generation, including source path,
                   destination path, filename, filter pattern, template configuration,
                   and precompilation options.
    :type params: FilesConfig
    :param project_path: The base path of the project where the customized files will be generated.
    :type project_path: Path
    :param force: If True, existing files will be overwritten. Default is False.
    :type force: bool
    
    :raises RuntimeError: If the destination folder does not exist.
    :raises TypeError: If the provided filename is not a string.
    :raises ValueError: If more than one source file is found and a filename is provided.

    :example:
    
        >>> params = FilesConfig(srcpath='src', destpath='output', filter_pattern='*.txt', 
        ...                       filename='custom_file.txt', templ_cfg={})
        >>> generate_files(params, Path('./project'))
        # This will generate customized files from 'src' to './project/output'.
    
    The function logs each step of the process for debugging purposes.
    '''
    srcfilespath = Path(params.srcpath)
    logger.debug(f'# Searching source files "{params.filter_pattern}" in location'
                 ' "{params.srcpath}" in progress...')

    # Checking folder path
    destpath = Path(str(project_path) + '/' + params.destpath)
    if not destpath.exists():
        errmsg = f'# Destination folder: "{destpath}" not exist.'
        logger.error(errmsg)
        raise RuntimeError(errmsg)

    # Source files list
    srcfiles = list(srcfilespath.glob(params.filter_pattern))
    if not srcfiles:
        logger.warning(f'# WARNING: Files "{params.filter_pattern}" not found in "{srcfilespath}".'
                       f'\n# Check source files and global paths. Files not generated.')
        return

    if params.filename and not isinstance(params.filename, str):
        raise TypeError(f'New file name must be a string (detected={type(params.filename)})')

    # Renaming multiple files at once doesn't make sense as pathlib.glob() order is arbitrary.
    if params.filename and len(srcfiles) > 1:
        errmsg = (f'New file name can be only used for a single file - not multiple '
                  f'(detected={len(srcfiles)})')
        logger.error(errmsg)
        raise ValueError(errmsg)

    logger.debug(f'# {len(srcfiles)} files found. Copying, customizing, & generating project files'
                 ' in progress...')

    # Generating and customizing new files
    for ff in srcfiles:
        fname = ff.name
        logger.debug(f'# Generating file "{fname}" in process...')

        if params.filename:
            logger.debug(f'# The file "{fname}" renamed to "{params.filename}".')
            fname = params.filename

        destfile = Path(str(project_path) + '/' + params.destpath + '/' + fname)
        if destfile.is_file() and not force:
            logger.debug(f'# The file "{fname}" exists in location "{destfile.as_posix()}"'
                         ' - not copied.')
            continue

        customize_template_file(destfile, ff, params.templ_cfg)

        # GCC precompiling
        if params.precompile_file:
            gccutils.precompile_file(destfile)

        logger.debug(f'# Generating file "{fname}" -> done.')
