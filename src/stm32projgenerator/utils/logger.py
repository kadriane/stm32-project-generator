'''
Custom colored logger
'''
import logging

# http://pueblo.sourceforge.net/doc/manual/ansi_color_codes.html
BASH_CLR_PREFIX = '\033['
BASH_CLR_SUFFIX = '\033[0m'
BASH_BOLD_PREFIX = ';1m'

LOGGER_FILE_FORMAT = ('%(asctime)s %(name)-15s %(levelname)-7s %(filename)-22s %(lineno)3d:'
                      ' %(message)s')


class ConsoleStreamFormatter(logging.Formatter):
    '''
    Custom formatter for colored console messages to stdout.

    This formatter applies colors to log messages based on their severity level,
    using ANSI escape codes for terminal output.

    :ivar colors: A dictionary mapping logging levels to corresponding ANSI color codes.
    :type colors: dict
    '''
    colors = {
        logging.CRITICAL: 41,   # white on red bg
        logging.FATAL: 41,      # white on red bg
        logging.ERROR: 31,      # red
        logging.WARNING: 33,    # yellow
        logging.WARN: 33,       # yellow
        logging.INFO: 36,       # cyan
        logging.DEBUG: 37,      # white
        logging.NOTSET: 30      # grey
    }

    def _colored_msg(self, msg: str, color: int) -> str:
        '''
        Format a message with the specified color.

        Compliant with Unix bash coding standard.
        
        Bash color format = '\033[{colorCode}{formatterCode}{message}\033[0m'

        :param msg: The message to be colored.
        :type msg: str
        :param color: The ANSI color code to apply.
        :type color: int
        :return: The formatted message with color codes.
        :rtype: str
        '''
        return f'{BASH_CLR_PREFIX}{color}{BASH_BOLD_PREFIX}{msg}{BASH_CLR_SUFFIX}'

    def format(self, record):
        """
        Format the log record and apply the appropriate color.

        :param record: The log record containing information about the event being logged.
        :type record: logging.LogRecord
        :return: The formatted log message with color applied.
        :rtype: str
        """
        color = self.colors[record.levelno]
        return self._colored_msg(record.msg, color)


def logger_setup(filename: str = "logger.log", debug: bool = False):
    '''
    Custom logger configurator for console to stdout and logging file.

    This function sets up the root logger with a console handler that outputs 
    colored messages and an optional file handler for saving logs to a file.

    :param filename: The name of the file to which logs should be written. If None,
                     the file handler will not be added.
    :type filename: str
    :param debug: If True, sets the log level to DEBUG; otherwise sets it to INFO.
                  Default is False.
    :type debug: bool
    
    :raises ValueError: If the filename is an empty string or invalid.
    
    :example:
    
        >>> logger_setup('my_log.log', debug=True)
        # This will set up a logger that outputs to both console and 'my_log.log' in debug mode.
    '''
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(ConsoleStreamFormatter())
    stream_handler.setLevel(logging.DEBUG if debug else logging.INFO)

    root.addHandler(stream_handler)

    if filename is None:
        root.warning('utils.logger_setup: file name is None - file handler not added.')
        return

    file_handler = logging.FileHandler(filename=filename, mode='w', encoding='utf-8')
    file_handler.setFormatter(logging.Formatter(LOGGER_FILE_FORMAT))
    file_handler.setLevel(logging.DEBUG)

    root.addHandler(file_handler)
