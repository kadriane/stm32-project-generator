FROM python:3.13-slim-bullseye

WORKDIR /stm32-project-generator

# PKG_VERSION is passed from CLI as a tag($CI_COMMIT_TAG for CI/CD jobs)
ARG PKG_VERSION="X.X.X"
ENV SETUPTOOLS_SCM_PRETEND_VERSION=${PKG_VERSION}

COPY src ./src
COPY pyproject.toml .
COPY MANIFEST.in .

ENV DEBIAN_FRONTEND noninteractive
RUN apt update
RUN apt install -y --no-install-recommends gcc

RUN python3 -m pip install .
