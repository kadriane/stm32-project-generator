'''
Test cases for STM32 MCU Family.
'''
import sys
import shutil
import unittest
import unittest.mock

from pathlib import Path

from stm32projgenerator.stm32projgenerator import main

SYS_ARGS_OK = {
	'stm32f103c6t6': [
		'stm32projgen',
		'stm32f103c6t6',
		'-n', 'stm32f103c6t6',
        '-f'
	],

	'stm32f103c8t6': [
		'stm32projgen',
		'stm32f103c8t6',
		'-n', 'stm32f103c8t6',
        '-f'
	],

	'stm32f407vgt6': [
		'stm32projgen',
		'stm32f407vgt6',
		'-n', 'stm32f407vgt6',
        '-f'
	],
}

# Selected
STM32_EXPECTED_FILES = {
    'stm32f103c6t6': [
        'STM32F103C6T6_LINKER.ld',
        'Makefile',
        'startup_stm32f103x6.s',

        'st/stm32f1xx.h',
        'st/stm32f103x6.h',
        'st/system_stm32f1xx.h',
        'st/system_stm32f1xx.c',

        'st/core/cmsis_armcc.h',
        'st/core/cmsis_gcc.h',
        'st/core/cmsis_version.h',

        'st/inc/stm32f1xx_hal.h',
        'st/inc/stm32f1xx_ll_cortex.h',
        'st/inc/stm32f1xx_hal_dma.h',

        'st/src/stm32f1xx_ll_adc.c',
        'st/src/stm32f1xx_ll_dma.c',
        'st/src/stm32f1xx_ll_gpio.c',

        'drivers/hw_system.c',
        'drivers/hw_irqn_handlers.h',
    ], # stm32f103c6t6

    'stm32f407vgt6': [
        'STM32F407VGT6_LINKER.ld',
        'Makefile',
        'startup_stm32f407xx.s',

        'st/stm32f4xx.h',
        'st/stm32f407xx.h',
        'st/system_stm32f4xx.h',
        'st/system_stm32f4xx.c',

        'st/core/cmsis_armcc.h',
        'st/core/cmsis_gcc.h',
        'st/core/cmsis_version.h',

        'st/inc/stm32f4xx_hal.h',
        'st/inc/stm32f4xx_ll_cortex.h',
        'st/inc/stm32f4xx_hal_dma.h',

        'st/src/stm32f4xx_ll_adc.c',
        'st/src/stm32f4xx_ll_dma.c',
        'st/src/stm32f4xx_ll_gpio.c',

        'drivers/hw_system.c',
        'drivers/hw_irqn_handlers.h',
    ] # stm32f407vgt6
} # STM32_EXPECTED_FILES


class STM32BaseTestCase(unittest.TestCase):
    '''
    STM32 Base Test Case with shared test methods and variables.
    '''
    def assert_files_exist(self, output_path: Path, expected_files: list):
        '''
        Check whether generated files exist.
        '''
        for fname in expected_files:
            ff = Path(f'{output_path}/{fname}')
            self.assertTrue(ff.exists(), msg=f'File "{ff}" does not exist!')

class STM32CommonTestCase(unittest.TestCase):
    '''
    STM32 Common Test Case for all MCUs.
    '''
    @unittest.mock.patch('sys.argv', ['stm32projgen', 'MCU_NOT_EXIST'])
    def test_mcu_family_not_exist(self):
        '''
        Test to check if ValueError exception is thrown for not supported MCU family.
        '''
        _msg = "STM32CommonTestCase: expected ValueError exception for not supported MCU family"
        with self.assertRaises(ValueError, msg=_msg):
            main()

class STM32F1xTestCase(STM32BaseTestCase):
    '''
    STM32 Test Case for F1 line.
    '''
    def setUp(self):
        '''
        Setup STM32 test cases by removing previously generated projects (if existed).
        '''
        self.mcu_name = 'stm32f103c6t6'
        self.output_path = f'out/{self.mcu_name}'
        self.expected_files = STM32_EXPECTED_FILES[self.mcu_name]

        shutil.rmtree(self.output_path, ignore_errors=True)

    def test_stm32f103c6t6(self):
        '''
        STM32F103C6T6 Test Case.
        '''
        sys.argv = SYS_ARGS_OK[self.mcu_name]
        main()
        self.assert_files_exist(output_path=self.output_path, expected_files=self.expected_files)

class STM32F4xTestCase(STM32BaseTestCase):
    '''
    STM32 Test Case for F4 line.
    '''
    def setUp(self):
        '''
        Setup STM32 test cases by removing previously generated projects (if existed).
        '''
        self.mcu_name = 'stm32f407vgt6'
        self.output_path = f'out/{self.mcu_name}'
        self.expected_files = STM32_EXPECTED_FILES[self.mcu_name]

        shutil.rmtree(self.output_path, ignore_errors=True)

    @unittest.mock.patch('sys.argv', SYS_ARGS_OK['stm32f407vgt6'])
    def test_stm32f407vgt6(self):
        '''
        STM32F407VGT6 Test Case.
        '''
        sys.argv = SYS_ARGS_OK[self.mcu_name]
        main()
        self.assert_files_exist(output_path=self.output_path, expected_files=self.expected_files)

if __name__ == '__main__':
    unittest.main()
