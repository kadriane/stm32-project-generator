'''
Fileutils general test cases
'''
import unittest

from pathlib import Path

from stm32projgenerator.utils.utils import load_yaml_file, file_search_all

from stm32projgenerator.utils import fileutils

from stm32projgenerator.generators.defs import FilesConfig, PKG_PATH, TEMPLATE_PATHS

class GenerateFilesTestCase(unittest.TestCase):
    '''
    Test Case for generating files using fileutils module.
    '''
    def setUp(self) -> None:
        self.make_config = load_yaml_file(f'{PKG_PATH}/config/stm32buildcfg.yaml')

        self.output_path = './out/test_fileutils'

        self.stm32rootpath = TEMPLATE_PATHS["stm32"]["libs"]

        self.thirdparty_path = Path(f'{self.stm32rootpath}/cmsis_device_f1/Source/'
                                    'Templates/gcc/linker')
        self.templates_path = f'{PKG_PATH}/templates/stm32/'

        mkcfg = self.make_config['stm32f103c6t6']['makefile']
        mkcfg['C_SOURCES'] = ''
        mkcfg['C_INCLUDES'] = ''

        self.mkfilecfg = FilesConfig(srcpath=self.templates_path,
									destpath='',
									filter_pattern='Makefile',
									templ_cfg=mkcfg)

        stm32f1xx_hal_path = Path(f'{self.stm32rootpath}/stm32f1xx_hal_driver/Src/')
        self.stm32f1xx_hal_sources = FilesConfig(srcpath=stm32f1xx_hal_path,
                                                 destpath='',
                                                 filter_pattern='*.c')

        fileutils.generate_folders(Path('test_fileutils'))

    def test_generate_files_renamed(self):
        '''
        Unit test for fileutils.generate_files() new names
        '''
        expected_fname = 'STM32F4XX_LINKER_NEW_NAME.ld'
        f4linkercfg = FilesConfig(srcpath=self.templates_path,
                                  destpath='',
                                  filter_pattern='STM32XXXX_LINKER.ld',
                                  templ_cfg=self.make_config['stm32f407vgt6']['linker'],
                                  filename=expected_fname,
                                  precompile_file=True)
        fileutils.generate_files(params=f4linkercfg,
                                 project_path=self.output_path)
        self.assertTrue(Path(f'{self.output_path}/{expected_fname}').exists())


    def test_generate_files(self):
        '''
        Unit test for fileutils.generate_files()
        '''
        filecfg_main = FilesConfig(self.templates_path, '', '*main*')

        linker_script_name = self.make_config['stm32f103c6t6']['makefile']['LD_SCRIPT_NAME']
        filecfg_linker = FilesConfig(srcpath=self.templates_path,
                                     destpath='',
                                     filter_pattern='STM32XXXX_LINKER.ld',
                                     templ_cfg=self.make_config['stm32f103c6t6']['linker'],
                                     filename=linker_script_name,
                                     precompile_file=True)

        fileutils.generate_files(params=filecfg_main,
                                 project_path=self.output_path)
        fileutils.generate_files(params=filecfg_linker,
                                 project_path=self.output_path)
        fileutils.generate_files(params=self.mkfilecfg,
                                 project_path=self.output_path)
        fileutils.generate_files(params=self.stm32f1xx_hal_sources,
                                 project_path=self.output_path)

        makefile_path = Path(f'{self.output_path}/Makefile')
        linker_path = Path(f'{self.output_path}/STM32F103C6T6_LINKER.ld')
        main_src_path = Path(f'{self.output_path}/main.c')
        main_header_path = Path(f'{self.output_path}/main.h')

        self.assertTrue(makefile_path.exists())
        self.assertTrue(linker_path.exists())
        self.assertTrue(main_src_path.exists())
        self.assertTrue(main_header_path.exists())

        linker_valid_substrs = [
            f'_estack = {filecfg_linker.templ_cfg["STACK_ADDR"]}',
            f'FLASH (rx) : ORIGIN = {filecfg_linker.templ_cfg["FLASH_ADDR"]},',
            f'RAM (xrw) : ORIGIN = {filecfg_linker.templ_cfg["RAM_ADDR"]},'
        ]

        makefile_valid_substrs = [
            f'TARGET = {self.mkfilecfg.templ_cfg["TARGET"]}',
            f'CPU = -mcpu={self.mkfilecfg.templ_cfg["MCU_CPU"]}',
            f'LDSCRIPT = {self.mkfilecfg.templ_cfg["LD_SCRIPT_NAME"]}'
        ]

        main_src_valid_substrs = [
            'hw_system_init();',
            '#include "main.h"'
        ]

        # Check if String Template substitution works well
        self.assertTrue(file_search_all(linker_path, linker_valid_substrs))
        self.assertTrue(file_search_all(makefile_path, makefile_valid_substrs))
        self.assertTrue(file_search_all(main_src_path, main_src_valid_substrs))

        # Selected
        stm32f1xx_hal_sources_expected = [
            'stm32f1xx_hal_nor.c',
            'stm32f1xx_hal_timebase_rtc_alarm_template.c',
            'stm32f1xx_hal_uart.c'
        ]

        for ff in stm32f1xx_hal_sources_expected:
            fpath = Path(f'{self.output_path}/{ff}')
            self.assertTrue(fpath.exists(), msg=f'File "{fpath}" not exists!')

        # Renaming files should only be possible for one single file being a string.
        with self.assertRaises(ValueError):
            params = self.stm32f1xx_hal_sources._replace(filename='this should be empty '
                                                                  'for multiple files')
            fileutils.generate_files(params=params,
                                     project_path=self.output_path)

        with self.assertRaises(TypeError):
            params = self.mkfilecfg._replace(filename=['this should not be a list'])
            fileutils.generate_files(params=params,
                                     project_path=self.output_path)


    def test_customize_template_file(self):
        '''
        Unit test for fileutils.customize_template_file()
        '''
        keys = load_yaml_file(f'{PKG_PATH}/config/stm32buildcfg.yaml')
        mk_fpath = Path(f'{self.output_path}/stm32_makefile')
        mktempl_fpath = Path(f'{PKG_PATH}/templates/stm32/Makefile')

        keys = keys['stm32f103c6t6']['makefile']
        keys['C_SOURCES'] = ''
        keys['C_INCLUDES'] = ''

        fileutils.customize_template_file(
            destfile=mk_fpath,
            srcfile=mktempl_fpath,
            params=keys
        )
        self.assertTrue(mk_fpath.exists())

        with self.assertRaises(FileNotFoundError):
            fileutils.customize_template_file(
                destfile=mk_fpath,
                srcfile=Path('/blabla/blablabla'),
                params={'blabla': 'bla'}
            )

        with self.assertRaises(KeyError):
            fileutils.customize_template_file(
                destfile=mk_fpath,
                srcfile=mktempl_fpath,
                params={'blabla': 'bla'}
            )

    def test_generate_files_filtering(self):
        '''
        Unit test for fileutils.generate_files() to check filtering.

        fileutils.generate_files() based on Path.glob() which regex is limited and not versatile
        as a standard regex from re library.
        '''
        stm32f4xx_hal_path = Path(f'{self.stm32rootpath}/stm32f4xx_hal_driver/Src/')
        srcs = FilesConfig(srcpath=stm32f4xx_hal_path,
                           destpath='',
                           filter_pattern='*_ll_*.c')

        src_single = FilesConfig(srcpath=stm32f4xx_hal_path,
                                 destpath='',
                                 filter_pattern='*_ll_adc.c')

        fileutils.generate_folders(Path('filtering/srcs'),
                                   destpath=self.output_path)
        fileutils.generate_folders(Path('filtering/single'),
                                   destpath=self.output_path)

        srcs_path = f'{self.output_path}/filtering/srcs'
        src_single_path = f'{self.output_path}/filtering/single'

        fileutils.generate_files(params=srcs,
                                 project_path=srcs_path)
        fileutils.generate_files(params=src_single,
                                 project_path=src_single_path)

        # Selected
        srcs_expected = [
            'stm32f4xx_ll_adc.c',
            'stm32f4xx_ll_rtc.c',
            'stm32f4xx_ll_usart.c'
        ]

        for ff in srcs_expected:
            fpath = Path(f'{srcs_path}/{ff}')
            self.assertTrue(fpath.exists(), msg=f'File "{fpath}" not exists!')

        # Expected precisely 1 file for the detailed filter
        fpath = Path(src_single_path)
        self.assertTrue(fpath.exists(), msg=f'File "{fpath}" not exists!')

        srcs_count = len(list(Path(srcs_path).glob('*')))
        src_single_count = len(list(Path(src_single_path).glob('*')))

        self.assertEqual(srcs_count,
                         22,
                         msg='Expected precisely 22 files for the basic filter.')
        self.assertEqual(src_single_count,
                         1,
                         msg='Expected precisely 1 file for the detailed filter.')

if __name__ == '__main__':
    unittest.main()
