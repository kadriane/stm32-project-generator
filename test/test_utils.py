'''
Test cases for utils module.
'''
import unittest
import importlib.resources

import stm32projgenerator
from stm32projgenerator.utils import utils

from stm32projgenerator.generators.stm32generator import STM32_MCU_CFG_REGEX

PKG_PATH = importlib.resources.files(stm32projgenerator)

class YamlLoaderTestCase(unittest.TestCase):
    '''
    Test case for YAML file loader.
    '''
    def test_load_file(self):
        '''
        A basic unit test for YAML file loader.
        '''
        data = utils.load_yaml_file(f'{PKG_PATH}/config/stm32buildcfg.yaml')
        self.assertIsInstance(data, dict)

        mcu = 'stm32f103c8t6'
        self.assertEqual(data[mcu]['linker']['FLASH_ADDR'], "0x08000000")
        self.assertEqual(data[mcu]['linker']['ALIGN_'], 8)

        self.assertEqual(data[mcu]['makefile']['MCU_CPU'], "cortex-M3")
        self.assertEqual(data[mcu]['makefile']['TARGET'], "stm32f103c8t6")

    def test_load_file_runtime_error(self):
        '''
        Unit test to check whether YAML loader function throws an exception if the file does
        not exists.
        '''
        with self.assertRaises(RuntimeError):
            utils.load_yaml_file('/blablabla/blablabla.yaml')


class CheckExecutablesTestCase(unittest.TestCase):
    '''
    Test case for utils.check_executables() function.
    '''
    def test_check_executables(self):
        '''
        Unit tests checking whether executables exist (or not).
        '''
        self.assertTrue(utils.check_executables(['find', 'python3']))
        self.assertFalse(utils.check_executables([]))
        self.assertFalse(utils.check_executables(['blabla', 'blablabla']))

        # args as a single string
        self.assertTrue(utils.check_executables('python3'))
        self.assertFalse(utils.check_executables('blabla'))


class FindSubstringTestCase(unittest.TestCase):
    '''
    Test case class for utils.find_substring() function.
    '''
    def test_findsubstring_basic(self):
        '''
        A basic set of unit tests for find_substring().
        '''
        self.assertIsNone(utils.find_substring('', 's32'))
        self.assertIsNone(utils.find_substring('', ''))
        self.assertIsNone(utils.find_substring('blablabla', ''))
        self.assertIsNone(utils.find_substring('', 'blablabla'))

    def test_findsubstring_regex(self):
        '''
        Unit tests for find_substring() using regex expressions.
        '''
        patterns = STM32_MCU_CFG_REGEX['series']

        self.assertEqual(utils.find_substring('STM32F031C6T6TR', patterns), 'F0')
        self.assertEqual(utils.find_substring('stm32f103c8t6', patterns), 'f1')
        self.assertEqual(utils.find_substring('STM32F373C8T6TR', patterns), 'F3')
        self.assertEqual(utils.find_substring('STM32F412RGY6TR', patterns), 'F4')
        self.assertEqual(utils.find_substring('STM32H747IIT6', patterns), 'H7')
        self.assertEqual(utils.find_substring('STM32L051K8U6', patterns), 'L0')
        self.assertEqual(utils.find_substring('STM32L4R7ZIT6', patterns), 'L4')
        self.assertEqual(utils.find_substring('STM32l471Rg', patterns), 'l4')
        self.assertEqual(utils.find_substring('STm32WB50CG', patterns), 'WB')
        self.assertEqual(utils.find_substring('STm32WB06KC', patterns), 'WB0')
        self.assertEqual(utils.find_substring('STm32Wb05TZ', patterns), 'Wb0')

        self.assertIsNone(utils.find_substring('', patterns))
        self.assertIsNone(utils.find_substring('STM32', patterns))
        self.assertIsNone(utils.find_substring('STM32Lbla4R7ZIT6', patterns))


if __name__ == '__main__':
    unittest.main()
