# STM32 Project Generator Makefile

PYTHON ?= venv/bin/python3
SPHINX_BUILD ?= venv/bin/sphinx-build

SRC_DIR ?= src/stm32projgenerator
DOCS_SRC_DIR ?= docs/source

# For docker image
PKG_VERSION ?= 0.0.0
DOCKER_IMG_NAME ?= "stm32-project-generator:${PKG_VERSION}"

# Ignore errors for linting.
# pylint is configured to fail if codebase is not perfect
.IGNORE: lint

.PHONY: all
all: pckg

clean:
	rm -rf dist/
	rm -rf build/
	rm -rf *.egg-info/
	rm -rf src/*.egg-info/
	rm -rf out/
	rm -rf .coverage

unittest:
	$(PYTHON) -m unittest -v

test: clean unittest

pckg_setup_py:
	$(PYTHON) setup.py bdist_wheel sdist

pckg_pyproject_toml:
	$(PYTHON) -m build .

pckg: clean pckg_pyproject_toml

code_style:
	$(PYTHON) -m pycodestyle $(SRC_DIR)

lint:
	$(PYTHON) -m pylint src/stm32projgenerator
	$(PYTHON) -m pylint test/

docs_html:
	$(PYTHON) -m sphinx -a -v -c $(DOCS_SRC_DIR) -b html $(DOCS_SRC_DIR) out/docs

docs: clean docs_html

coverage: clean
	$(PYTHON) -m coverage run -a --source=stm32projgenerator -m unittest -vv
	$(PYTHON) -m coverage report -m --precision=1

docker_build:
	docker build --build-arg PKG_VERSION=$(PKG_VERSION) -t $(DOCKER_IMG_NAME) .

docker_run_test:
	docker run -v ./out:/tmp $(DOCKER_IMG_NAME) stm32projgen stm32f103c8t6 -f -n test -p /tmp

EXE_DATA = --add-data "src/stm32projgenerator/templates:stm32projgenerator/templates"
EXE_DATA += --add-data "src/stm32projgenerator/config:stm32projgenerator/config"
EXE_DATA += --add-data "src/stm32projgenerator/thirdparty:stm32projgenerator/thirdparty"

exe: clean
	$(PYTHON) -m PyInstaller $(EXE_DATA) --name=stm32projgen --onefile $(SRC_DIR)/__main__.py --copy-metadata stm32projgenerator
