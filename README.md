[![Latest Release](https://gitlab.com/kadriane/stm32-project-generator/-/badges/release.svg)](https://gitlab.com/kadriane/stm32-project-generator/-/releases) 

[![pipeline status](https://gitlab.com/kadriane/stm32-project-generator/badges/master/pipeline.svg)](https://gitlab.com/kadriane/stm32-project-generator/-/commits/master) 

[![coverage report](https://gitlab.com/kadriane/stm32-project-generator/badges/master/coverage.svg)](https://gitlab.com/kadriane/stm32-project-generator/-/commits/master)


# STM32 Project Generator

Template generator for STM32 and other MCUs.

## General Info
This project is a template generator for different MCUs based on generic makefiles, customized C files, and open source MCU-vendor libs for Linux OS. 

The first version is dedicated for the STM32 family only, but the project can be easily extended to other MCUs.

The generator uses only HAL LL(Low Layer) source files from ST packages.

Currently, it is possible to generate configuration files for only one IDE: Visual Studio Code.

## Documentation

A basic documentation can be found [here](https://kadriane.gitlab.io/stm32-project-generator/).

## License
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details.

## Author
- **Karol Adrianek**: (gitlab:kadriane)

## Acknowledgments
- **STMicroelectronics**  - patches for HAL, CMSIS libraries and templates for a makefile and linker scripts:
  - [STM32 CMSIS core v5.9.0](https://github.com/STMicroelectronics/cmsis_core/tree/v5.9.0),
  - [STM32F1xx HAL libraries v1.1.9](https://github.com/STMicroelectronics/stm32f1xx_hal_driver/tree/v1.1.9),
  - [STM32F4xx HAL libraries v1.8.3](https://github.com/STMicroelectronics/stm32f4xx_hal_driver/tree/v1.8.3),
  - [STM32F1 CMSIS v4.3.4](https://github.com/STMicroelectronics/cmsis_device_f1/tree/v4.3.4),
  - [STM32F4 CMSIS v2.6.10](https://github.com/STMicroelectronics/cmsis_device_f4/tree/v2.6.10).